/*
| Initializes the ti2c-ot package.
*/
import ti2c from 'ti2c';
await ti2c.register(
	'name',    'ot',
	'meta',    import.meta,
	'source',  'src/',
	'relPath', 'init'
);
