/*
| Collection of generic change functions.
*/
def.abstract = true;

import { Self as Sequence  } from '{Change/Sequence}';
import { Self as Simulcast } from '{Change/Simulcast}';

/*
| Returns a 'change' transformed on this.
*/
def.proto.transform =
	function( other )
{
	if( !other ) return other;

	const t = this.ti2ctype._transformers.get( other.ti2ctype );

	if( !t )
	{
		throw new Error( this.__TI2C_NAME__ + ' has no transformer for ' + other.__TI2C_NAME__ );
	}

	return this[ t ]( other );
};

/*
| By default traces are undefined.
| (to be set undefined for ti2c property trap checking).
|
| FIXME really? why?
*/
def.proto.trace     = undefined;
def.proto.traceFrom = undefined;
def.proto.traceTo   = undefined;

/*
| Returns a change sequence transformed by this change.
*/
def.proto._transformSequence =
	function( other )
{
/**/if( CHECK && other.ti2ctype !== Sequence ) throw new Error( );

	return this._transformSimulcast( other.simulcast ).sequence;
};

/*
| Returns a change simlucast transformed by this change.
*/
def.proto._transformSimulcast =
	function( other )
{
/**/if( CHECK && other.ti2ctype !== Simulcast ) throw new Error( );

	const list = [ ];
	for( let co of other )
	{
		const cot = this.transform( co );

		if( !cot ) continue;

/**/	if( CHECK && cot.ti2ctype === Sequence ) throw new Error( );

		if( cot.ti2ctype === Simulcast )
		{
			for( let c of cot ) list.push( c );
		}
		else
		{
			list.push( cot );
		}
	}

	return Simulcast.Array( list );
};

/*
| Returns a non transform.
*/
def.proto._transformSame =
	( o ) => o;
