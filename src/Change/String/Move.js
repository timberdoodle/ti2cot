/*
| A string is moved, or inserted, or removed.
*/
def.extend = 'Change/Base';

def.attributes =
{
	// if true does not transform traces on the edge of the from trace
	slippyFrom: { type: [ 'undefined', 'boolean' ], json: true },

	// if true does not transform traces on the edge of the to trace
	slippyTo: { type: [ 'undefined', 'boolean' ], json: true },

	// move from this trace
	// undefined in case of insertion only
	traceFrom: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// move to this trace
	// undefined in case of removal only
	traceTo: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// moving this contents
	val: { type: 'string', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeError    } from '{Change/Error}';
import { Self as ListMove       } from '{Change/List/Move}';
import { Self as TreeAssign     } from '{Change/Tree/Assign}';
import { Self as TreeGrow       } from '{Change/Tree/Grow}';
import { Self as TreeShrink     } from '{Change/Tree/Shrink}';
import { Self as Sequence       } from '{Change/Sequence}';
import { Self as Simulcast      } from '{Change/Simulcast}';
import { Self as Trace          } from '{ti2c:Trace}';

/*
| Turns the change into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	const from = this.traceFrom;
	const to = this.traceTo;

	let str = 'change(StringMove):\n';
	str += '  traceFrom: ' + ( from ? from.asString : 'undefined' ) + '\n';
	str += '  traceTo: ' + ( to ? to.asString : 'undefined' ) + '\n';
	str += '  val: ' + this.val + '\n';
	return str;
};

/*
| Performs the change on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	const from = this.traceFrom;
	const to = this.traceTo;
	const val = this.val;
	const vlen = val.length;

	let strFrom = from?.back.pick( tree );
	let strTo = to?.back.pick( tree );

	if( from && typeof( strFrom ) !== 'string' )
	{
		throw ChangeError.make( 'StringMove.traceFrom yields no string' );
	}

	if( to && typeof( strTo ) !== 'string' )
	{
		throw ChangeError.make( 'StringMove.traceTo yields no string' );
	}

	this.check( strFrom, strTo );

	if( from && to && from.back === to.back )
	{
/**/	if( CHECK && strFrom !== strTo ) throw new Error( );

		const atFrom = from.last.at;
		strFrom = strFrom.substring( 0, atFrom ) + strFrom.substring( atFrom + vlen );

		let atTo = to.last.at;
		if( atTo > atFrom )
		{
			atTo -= val.length;
		}

		strFrom = strFrom.substring( 0, atTo ) + val + strFrom.substring( atTo );

		return from.back.graft( tree, strFrom );
	}

	if( from )
	{
		const atFrom = from.last.at;
		strFrom = strFrom.substring( 0, atFrom ) + strFrom.substring( atFrom + vlen );
		tree = from.back.graft( tree, strFrom );
	}

	if( to )
	{
		const atTo = to.last.at;
		strTo = strTo.substring( 0, atTo ) + val + strTo.substring( atTo );
		tree = to.back.graft( tree, strTo );
	}

	return tree;
};

/*
| Additional checks about basic sanity.
|
| ~strFrom, strTo: optional. if defined, tests sanity against these strings.
*/
def.proto.check =
	function( strFrom, strTo )
{
	const from = this.traceFrom;
	const to = this.traceTo;
	const val = this.val;
	const vlen = val.length;

	const atFrom = from?.last.at;
	const atTo = to?.last.at;

	if( from )
	{
		if( atFrom < 0 )
		{
			throw ChangeError.make( 'StringMove.traceFrom negative from' );
		}

		if( strFrom )
		{
			if( atFrom > strFrom.length )
			{
				throw ChangeError.make( 'StringMove.traceFrom outside of string' );
			}

			if( strFrom.substr( atFrom, vlen ) !== val )
			{
				throw ChangeError.make( 'StringMove.val wrong' );
			}
		}
	}

	if( to )
	{
		if( atTo < 0 )
		{
			throw ChangeError.make( 'StringMove.traceTo negative from' );
		}

		if( strTo && atTo > strTo.length )
		{
			throw ChangeError.make( 'StringMove.traceTo outside of string' );
		}
	}

	if( from && to && from.back === to.back )
	{
		if( atTo > atFrom && atTo <= atFrom + vlen )
		{
			throw ChangeError.make( 'StringMove.traceTo inside "from"-range' );
		}
	}
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	return(
		this.create(
			'traceFrom', this.traceFrom?.chop,
			'traceTo', this.traceTo?.chop,
		)
	);
};

/*
| Explodes this change into a list of moves of single char sizes only.
| To be applied in sequence.
*/
def.lazy.explodeSequence =
	function( )
{
	const val = this.val;
	const vlen = val.length;

	// already exploded?
	if( vlen <= 1 ) return this;

	const from = this.traceFrom;
	const to = this.traceTo;
	const list = [ ];

	if(
		from
		&& ( !to || from.lastAtIsBefore( to ) )
	)
	{
		// exploding a remove:
		//      from
		// <--->123<--->
		// =>
		// <--->1::<--->
		// <--->2:<--->
		// <--->3<--->
		// from stays the same, value is iterated
		//
		// or
		//
		// and in sentence where from is before to
		//      from    to
		// <--->123<---><--->
		// =>
		// <--->1::<--->1<--->
		// <--->2:<--->:2<--->
		// <--->3<--->::3<--->
		// from and to stay the same.
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create( 'val', val.charAt( v ) )
			);
		}
	}
	else if( to.lastAtIsBefore( from ) || to === from )
	{
		// splitting an in sentence from-to where from is after the to
		//      to   from
		// <---><--->123<--->
		// =>
		// <---><--->1::<--->
		// <--->1<--->2:<--->
		// <--->12<--->3<--->
		// from and to increase
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create(
					'traceFrom', from.incLastAt( v ),
					'traceTo', to.incLastAt( v ),
					'val', val.charAt( v ),
				)
			);
		}
	}
	else
	{
		// from and to are in different lines
		//
		// or
		//
		// exploding an insert:
		//      to
		// <--->123<--->
		// =>
		// <--->1<--->
		// <--->:2<--->
		// <--->::3<--->
		// to increases while the value is iterated
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create(
					'traceTo', to.incLastAt( v ),
					'val', val.charAt( v ),
				)
			);
		}
	}

	return Sequence.Array( list );
};

/*
| Explodes this change into a list of moves of single char sizes only.
| All changes are to be considered simultaneous.
*/
def.lazy.explodeSimulcast =
	function( )
{
	const val = this.val;
	const vlen = val.length;

	// already exploded?
	if( vlen <= 1 ) return this;

	const from = this.traceFrom;
	const to = this.traceTo;
	const list = [ ];

	if( from && !to )
	{
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create(
					'traceFrom', from.incLastAt( v ),
					'val', val.charAt( v )
				)
			);
		}
	}
	else if( !from && to )
	{
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create(
					'val', val.charAt( v ),
				)
			);
		}
	}
	else
	{
		for( let v = 0; v < vlen; v++ )
		{
			list.push(
				this.create(
					'traceFrom', from.incLastAt( v ),
					//'traceTo', to.incLastAt( v ),
					'val', val.charAt( v ),
				)
			);
		}
	}

	return Simulcast.Array( list );
};

/*
| True if the change is a useless do-thing operation.
*/
def.lazy.isNoop =
	function( )
{
	const from = this.traceFrom;
	const to = this.traceTo;

	if( !from || !to || from.back !== to.back ) return false;

	const atFrom = from?.last.at;
	const atTo = to?.last.at;

	return atTo === atFrom || atTo === atFrom + this.val.length;
};

/*
| Returns true if this move intersects with another move.
*/
def.proto.intersects =
	function( other )
{
/**/if( CHECK && other.ti2ctype !== Self ) throw new Error( );

	const vlenThis  = this.val.length;
	const vlenOther = other.val.length;

	const fromThis  = this.traceFrom;
	const toThis    = this.traceTo;
	const fromOther = other.traceFrom;
	const toOther   = other.traceTo;

	const atFromThis  = fromThis?.last.at;
	const atToThis    = toThis?.last.at;
	const atFromOther = fromOther?.last.at;
	const atToOther   = toOther?.last.at;

	if( fromThis && fromOther
		&& fromThis.back === fromOther.back
		&& (
			atFromThis >= atFromOther && atFromThis <= atFromOther + vlenOther
			|| atFromOther >= atFromThis && atFromOther <= atFromThis + vlenThis
		)
	) return true;

	if( fromThis && toOther
		&& fromThis.back === toOther.back
		&& (
			atFromThis >= atToOther && atFromThis <= atToOther + vlenOther
			|| atToOther >= atFromThis && atToOther <= atFromThis + vlenThis
		)
	) return true;

	if( toThis && fromOther
		&& toThis.back === fromOther.back
		&& (
			atToThis >= atFromOther && atToThis <= atFromOther + vlenOther
			|| atFromOther >= atToThis && atFromOther <= atToThis + vlenThis
		)
	) return true;

	if( toThis && toOther
		&& toThis.back === toOther.back
		&& (
			atToThis >= atToOther && atToThis <= atToOther + vlenOther
			|| atToOther >= atToThis && atToOther <= atToThis + vlenThis
		)
	) return true;

	return false;
};

/*
| Returnis the inversion to this change.
*/
def.lazy.reversed =
	function( )
{
	let from = this.traceFrom;
	let to = this.traceTo;

	// special case from and to are in the same line:
	// for example:
	//   012345
	//   Sphinx
	//
	// from 1 to 4:
	//   012345
	// ->Shipnx
	//
	// reverse needs to be
	// from 3 to 1
	//
	if( from && to )
	{
		if( from.lastAtIsBefore( to ) )
		{
			to = to.incLastAt( -this.val.length );
		}
		else if( to.lastAtIsBefore( from ) )
		{
			from = from.incLastAt( this.val.length );
		}
	}

	return(
		this.create(
			'slippyFrom', this.slippyTo,
			'slippyTo',   this.slippyFrom,
			'traceFrom',  to,
			'traceTo',    from,
		)
	);
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
	if( !this.traceFrom && !this.traceTo ) throw new Error( );

	//actually empty strings are allowed, for trace (caret) transformations.
	//if( this.val === '' ) throw new Error( );
};

/*
| Maps transformables to transform functions
*/
def.staticLazy._transformers =
	( ) =>
{
	const map = new Map( );
	map.set( ListMove,       '_transformListMove' );
	map.set( Self,           '_transformStringMove' );
	map.set( Sequence,       '_transformSequence' );
	map.set( Simulcast,      '_transformSimulcast' );
	map.set( TreeAssign,     '_transformSame' );
	map.set( TreeGrow,       '_transformSame' );
	map.set( TreeShrink,     '_transformSame' );
	map.set( Trace,          '_transformTrace' );
	return map;
};

/*
| Transforms a ListMove change
| considering this StringMove actually came first.
|
| Possible it's value needs to be changed.
*/
def.proto._transformListMove =
	function( other )
{
/**/if( CHECK && other.ti2ctype !== ListMove ) throw new Error( );

	const fromO = other.traceFrom;
	const fromT = this.traceFrom;
	const toT   = this.traceTo;

	const valT = this.val;
	let valO = other.val;
	let moved = 0;

	if( fromO )
	{
		if( fromT?.hasTrace( fromO ) )
		{
			if( fromO.length + 2 !== fromT.length )
			{
				// XXX TODO
				throw new Error( );
			}

			let strO = valO.string;
			const atFromT = fromT.last.at;

			strO =
				strO.substring( 0, atFromT )
				+ strO.substring( atFromT + valT.length );

			valO = valO.create( 'string', strO );
			moved = valT.length;
		}

		if( toT?.hasTrace( fromO ) )
		{
			if( fromO.length + 2 !== toT.length )
			{
				// XXX TODO
				throw new Error( );
			}

			let strO = valO.string;
			let atToT = toT.last.at;

			if( moved > 0 && atToT > fromT.last.at )
			{
				atToT -= moved;
			}

			strO =
				strO.substring( 0, atToT )
				+ valT
				+ strO.substring( atToT );

			valO = valO.create( 'string', strO );
		}
	}

	return other.create( 'val', valO );
};

/*
| Transforms another StringMove change
| considering this StringMove actually came first.
*/
def.proto._transformStringMove =
	function( other )
{
/**/if( CHECK && other.ti2ctype !== Self ) throw new Error( );

	if( this.intersects( other ) )
	{
		if( this.val.length > 1 )
		{
			return this.explodeSequence.transform( other );
		}

		if( other.val.length > 1 )
		{
			return this.transform( other.explodeSimulcast );
		}
	}

	const fromThis = this.traceFrom;
	const toThis = this.traceTo;
	const valThis = this.val;
	const valThisLen = valThis.length;
	const fromOther = other.traceFrom;
	const toOther = other.traceTo;
	const valOther = other.val;
	let fromOtherT = fromOther;
	let toOtherT = toOther;

	// checks if the other change moves 'from' the same place
	// however ignore this, if this val is less than the other
	// (since overlapping changes are split, this should except zero size
	//  moves only)
	if( fromThis && fromThis === fromOther && valThis.length >= valOther.length )
	{
		if( toThis === toOther )
		{
			// identical changes are eaten
			return undefined;
		}
		else if( toThis )
		{
			// both have the same from
			// thus the 'from' of transformed change goes
			// to the 'to' of this.
			fromOtherT = toThis;
			if( fromThis.lastAtIsBefore( toThis ) )
			{
				fromOtherT = fromOtherT.incLastAt( -valThisLen );
			}

			if( fromOther.lastAtIsBefore( toOther ) )
			{
				toOtherT = toOtherT.incLastAt( -valThisLen );
			}

			const otherT =
				other.create(
					'traceFrom', fromOtherT,
					'traceTo', toOtherT,
					'val', valOther,
				);

			return !otherT.isNoop ? otherT : undefined;
		}
		else
		{
			// this is a remove so its the other move
			return undefined;
		}
	}

	if( fromThis )
	{
		if( fromThis.lastAtIsBefore( fromOther ) )
		{
			fromOtherT = fromOtherT.incLastAt( -valThisLen );
		}

		if( fromThis.lastAtIsBefore( toOther ) )
		{
			toOtherT = toOtherT.incLastAt( -valThisLen );
		}
	}

	if( toThis )
	{
		if( toThis === fromOther || toThis.lastAtIsBefore( fromOther ) )
		{
			fromOtherT = fromOtherT.incLastAt( valThisLen );
		}

		if( toThis === toOther || toThis.lastAtIsBefore( toOther ) )
		{
			toOtherT = toOtherT.incLastAt( valThisLen );
		}
	}

	const otherT =
		other.create(
			'traceFrom', fromOtherT,
			'traceTo', toOtherT,
			'val', valOther,
		);

	return !otherT.isNoop ? otherT : undefined;
};

/*
| Transforms a trace by this change.
*/
def.proto._transformTrace =
	function( trace )
{
/**/if( CHECK && trace.ti2ctype !== Trace ) throw new Error( );

	// anything that is not an offset into a string is not changed
	const traceLast = trace.last;
	if( traceLast.name !== 'offset' ) return trace;

	let atTrace = traceLast.at;
	const from = this.traceFrom;
	const to = this.traceTo;
	const val = this.val;
	const vlen = val.length;
	let moved;

	if( from?.back === trace.back )
	{
		const atFromThis = from.last.at;

		// if the trace is before the from-range it doesn't change
		if(
			atFromThis > atTrace
			|| atFromThis === this.slippyFrom
		)
		{
			// nothing
		}
		else
		{
			if(
				atFromThis + vlen > atTrace
				|| atFromThis + vlen === atTrace && !this.slippyFrom
			)
			{
				// its within the from-area
				if( to )
				{
					// it moves to the new to-area
					atTrace = to.last.at + atTrace - atFromThis;
					trace = to.back.add( 'offset', atTrace );
					moved = true;
				}
				else
				{
					// it was a plain remove, the trace moves
					// to the begin of removed range
					atTrace = atFromThis;
					trace = trace.back.add( 'offset', atTrace );
				}
			}
			else
			{
				// the trace was after the 'from' range, thus moves to left
				atTrace -= vlen;
				trace = trace.back.add( 'offset', atTrace );
			}
		}
	}

	if( !moved && to?.back === trace.back )
	{
		const atTo = to.last.at;

		// if the trace is before the to-spot it doesn't change
		if(
			atTo > atTrace
			|| ( atTo === atTrace && this.slippyTo )
		)
		{
			// nothing
		}
		else
		{
			// otherwise it is moved to the right
			trace = trace.back.add( 'offset', atTrace + vlen );
		}
	}

	return trace;
};
