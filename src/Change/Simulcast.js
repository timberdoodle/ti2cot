/*
| A list of changes to be applied simultaneously.
|
| Note that simulcasts cannot transform other changes,
|      they need to be converted into sequences first.
*/
def.extend = 'list@<Change/Types';
def.json = true;

import { Self as Sequence } from '{Change/Sequence}';

/*
| Turns the change into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	let str = 'simulcast [\n';
	for( let change of this )
	{
		str += change.asString;
	}
	str += ']\n';
	return str;
};

/*
| Transforms the list such that all changes are
| chronological.
*/
def.lazy.sequence =
	function( )
{
	let list = this.clone( );
	const len = this.length;

	for( let a = 0; a < len; a++ )
	{
		const ca = list[ a ];
		for( let b = a + 1; b < len; b++ )
		{
			const cb = list[ b ];
			list[ b ] = ca.transform( cb );
		}
	}

	//list = list.filter( ( e ) => e !== undefined );
	for( let a = 0, alen = list.length; a < alen; a++ )
	{
		let e = list[ a ];

		if( e === undefined )
		{
			list.splice( a, 1 );
			alen--;
			continue;
		}

		if( e.ti2ctype === Self )
		{
			e = e.sequence;
		}

		if( e.ti2ctype === Sequence )
		{
			const l2 = e.clone( );
			list.splice( a, 1, ...l2 );
			const skip = l2.length - 1;
			a += skip;
			alen += skip;
		}
	}

	return Sequence.Array( list );
};

/*
| If it is already a simulcast, this is a noop.
*/
def.lazy.simulcast =
	function( )
{
	return this;
};

/*
| Returns a change list with reversed changes.
*/
def.lazy.reversed =
	function( )
{
	const list = [ ];
	for( let c of this.reverse( ) )
	{
		list.push( c.reversed );
	}
	const result = Self.Array( list );
	ti2c.aheadValue( result, 'reversed', this );
	return result;
};
