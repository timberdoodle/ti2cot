/*
| Moves into or out of a list.
*/
def.extend = 'Change/Base';

def.attributes =
{
	// move from this trace
	// undefined in case of insertion only
	traceFrom: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// move to this trace
	// undefined in case of removal only
	traceTo: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// value to insert
	val: { type: [ '< Change/Vals' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeError    } from '{Change/Error}';
import { Self as Sequence       } from '{Change/Sequence}';
import { Self as Simulcast      } from '{Change/Simulcast}';
import { Self as StringMove     } from '{Change/String/Move}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TreeAssign     } from '{Change/Tree/Assign}';
import { Self as TreeGrow       } from '{Change/Tree/Grow}';
import { Self as TreeShrink     } from '{Change/Tree/Shrink}';

/*
| Turns the change into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	const from = this.traceFrom;
	const to = this.traceTo;

	let str = 'change(ListMove):\n';
	str += '  traceFrom: ' + ( from ? from.asString : 'undefined' ) + '\n';
	str += '  traceTo: ' + ( to ? to.asString : 'undefined' ) + '\n';
	str += '  val: ' + this.val + '\n';
	return str;
};

/*
| Performs the list app change on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	const from = this.traceFrom;
	const to = this.traceTo;
	const lastFrom = from?.last;
	const lastTo = to?.last;

	const val = this.val;

	if( from && to && from.back === to.back && lastFrom.name === lastTo.name )
	{
		const atFrom = lastFrom.at;
		const name = lastFrom.name;

		let base = from.back.pick( tree );
		let list = base[ name ];

		if( list.get( atFrom ) !== val )
		{
			throw ChangeError.make( 'ListMove.val wrong' );
		}
		list = list.remove( atFrom );

		let atTo = lastTo.at;
		if( atTo > atFrom )
		{
			atTo--;
		}

		list = list.insert( atTo, val );
		base = base.create( name, list );
		return from.back.graft( tree, base );
	}

	if( from )
	{
		const at = lastFrom.at;
		const name = lastFrom.name;

		let base = from.back.pick( tree );
		let list = base[ name ];
		if( list.get( at ) !== val )
		{
			throw ChangeError.make( 'ListMove.val wrong' );
		}
		list = list.remove( at );
		base = base.create( name, list );
		tree = from.back.graft( tree, base );
	}

	if( to )
	{
		const at = lastTo.at;
		const name = lastTo.name;

		let base = to.back.pick( tree );
		let list = base[ name ];
		list = list.insert( at, val );
		base = base.create( name, list );
		tree = to.back.graft( tree, base );
	}

	return tree;
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	return(
		this.create(
			'traceFrom', this.traceFrom?.chop,
			'traceTo', this.traceTo?.chop,
		)
	);
};

/*
| Returns the inversion to this change.
*/
def.lazy.reversed =
	function( )
{
	let from = this.traceFrom;
	let to = this.traceTo;

	if( from && to )
	{
		if( from.lastAtIsBefore( to ) )
		{
			to = to.incLastAt( -this.val.length );
		}
		else if( to.lastAtIsBefore( from ) )
		{
			from = from.incLastAt( this.val.length );
		}
	}

	return(
		this.create(
			'traceFrom',  to,
			'traceTo',    from,
		)
	);
};

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( json.$type !== this.$type ) throw new Error( );

	const fromJ = json.traceFrom;
	const toJ   = json.traceTo;

	const from = fromJ && Trace.FromJson( fromJ, plan );
	const to   = toJ   && Trace.FromJson( toJ,   plan );

	const val =
		from
		? from.last.fromJsonVar( json, plan, 'val' )
		: to.last.fromJsonVar( json, plan, 'val' );

	return(
		Self.create(
			'traceFrom', from,
			'traceTo', to,
			'val', val,
		)
	);
};

/*
| Maps transformables to transform functions
*/
def.staticLazy._transformers =
	( ) =>
{
	const map = new Map( );
	map.set( Self,             '_transformListMove'         );
	map.set( StringMove,       '_transformStringMove'       );
	map.set( Sequence,         '_transformSequence'         );
	map.set( Simulcast,        '_transformSimulcast'        );
	map.set( Trace,            '_transformTrace'            );
	map.set( TreeGrow,         '_transformSame'             );
	map.set( TreeShrink,       '_transformSame'             );
	map.set( TreeAssign,       '_transformSame'             );
	return map;
};

/*
| Transforms a change
| considering this insert actually came first.
*/
def.proto._transformListMove =
	function( c )
{
/**/if( CHECK && c.ti2ctype !== Self ) throw new Error( );

	let traceFrom = c.traceFrom;
	let traceTo   = c.traceTo;

	if( traceFrom )
	{
		traceFrom = this._transformTrace( traceFrom );
	}

	if( traceTo )
	{
		traceTo = this._transformTrace( traceTo );
	}

	if( traceFrom || traceTo )
	{
		return c.create( 'traceFrom', traceFrom, 'traceTo', traceTo );
	}
	else
	{
		return undefined;
	}
};

/*
| Transforms a change
| considering this insert actually came first.
*/
def.proto._transformStringMove =
	function( c )
{
/**/if( CHECK && c.ti2ctype !== StringMove ) throw new Error( );

	let traceFrom = c.traceFrom;
	let traceTo   = c.traceTo;

	if( traceFrom )
	{
		traceFrom = this._transformTrace( traceFrom );
	}

	if( traceTo )
	{
		traceTo = this._transformTrace( traceTo );
	}

	if( traceFrom || traceTo )
	{
		return c.create( 'traceFrom', traceFrom, 'traceTo', traceTo );
	}
	else
	{
		return undefined;
	}
};

/*
| Transforms a trace by this change.
*/
def.proto._transformTrace =
	function( trace )
{
/**/if( CHECK && trace.ti2ctype !== Trace ) throw new Error( );

	const from = this.traceFrom;
	const to = this.traceTo;
	let moved = 0;

	if(
		from
		&& trace.back.hasTrace( from.back )
		&& trace.get( from.length - 1 ).name === from.last.name
	)
	{
		const lenFrom = from.length;
		const lastFrom = from.last;
		const atFrom = lastFrom.at;
		let stepTraceLF = trace.get( lenFrom - 1 );
		let atTraceLF = stepTraceLF.at;

		if( stepTraceLF.name === lastFrom.name )
		{
			if( atTraceLF === atFrom ) return undefined;

			if( atTraceLF > atFrom )
			{
				stepTraceLF = stepTraceLF.create( 'at', --atTraceLF );
				trace = trace.setStep( lenFrom - 1, stepTraceLF );

				if( to && from.back === to.back )
				{
					moved = -1;
				}
			}
		}
	}

	if(
		to
		&& trace.back.hasTrace( to.back )
		&& trace.get( to.length - 1 ).name === to.last.name
	)
	{
		const lenTo = to.length;
		const lastTo = to.last;
		const atTo = lastTo.at;

		let stepTraceLT = trace.get( lenTo - 1 );
		let atTraceLT = stepTraceLT.at;

		if( stepTraceLT.name === lastTo.name )
		{
			if( atTraceLT >= atTo + moved )
			{
				stepTraceLT = stepTraceLT.create( 'at', ++atTraceLT + moved );
				trace = trace.setStep( lenTo - 1, stepTraceLT );
			}
		}
	}

	return trace;
};
