/*
| Assigns a tree node.
*/
def.extend = 'Change/Base';

def.attributes =
{
	// the value the tree had
	prev: { type: [ '< Change/Vals' ], json: true },

	// assign at this trace
	trace: { type: 'ti2c:Trace', json: true },

	// value to assign
	val: { type: [ '< Change/Vals' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ListMove       } from '{Change/List/Move}';
import { Self as Sequence       } from '{Change/Sequence}';
import { Self as Simulcast      } from '{Change/Simulcast}';
import { Self as StringMove     } from '{Change/String/Move}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TreeGrow       } from '{Change/Tree/Grow}';
import { Self as TreeShrink     } from '{Change/Tree/Shrink}';

/*
| Performs the insertion change on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	// Stores the old value for history tracking.
	const prev = this.trace.pick( tree );

	if(
		prev !== this.prev
		&& (
			prev.jsonfy
			&& this.prev.jsonfy( )
			&& prev.jsonfy( ) !== this.prev.jsonfy( )
		)
	)
	{
		throw new Error( 'set.prev doesn\'t match' );
	}

	return this.trace.graft( tree, this.val );
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	return this.create( 'trace', this.trace?.chop );
};

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );
	if( json.$type !== this.$type ) throw new Error( );

	const trace = Trace.FromJson( json.trace, plan );
	const prev = trace.last.fromJsonVar( json, plan, 'prev' );
	const val = trace.last.fromJsonVar( json, plan, 'val' );

	return Self.create( 'trace', trace, 'prev', prev, 'val', val );
};

/*
| Returns the inversion to this change.
*/
def.lazy.reversed =
	function( )
{
	const inv = Self.create( 'trace', this.trace, 'val', this.prev, 'prev', this.val );
	ti2c.aheadValue( inv, 'reversed', this );
	return inv;
};

/*
| Transforms a trace by this set.
*/
def.proto._transformTrace =
	function( trace )
{
	// TODO make it more flexible than just items
	return(
		this.trace !== trace.backward( 'items' )
		? trace
		: undefined
	);
};

/*
| Maps transformables to transform functions
*/
def.staticLazy._transformers =
	( ) =>
{
	const map = new Map( );
	map.set( ListMove,       '_transformSame'           );
	map.set( Self,           '_transformChangeSet'      );
	map.set( Sequence,       '_transformSequence'       );
	map.set( Simulcast,      '_transformSimulcast'      );
	map.set( StringMove,     '_transformStringMove'     );
	map.set( Trace,          '_transformTrace'          );
	map.set( TreeGrow,       '_transformSame'           );
	map.set( TreeShrink,     '_transformSame'           );
	return map;
};

/*
| Transforms a set
| by this set actually happening first.
*/
def.proto._transformChangeSet =
	function( cx )
{
	if( this.trace !== cx.trace ) return cx;

	// TODO calls into vars  ???
	if(
		cx.prev === this.prev
		|| ( cx.prev && this.prev && cx.prev.jsonfy( ) === this.prev.jsonfy( ) )
	)
	{
		return cx.create( 'prev', this.val );
	}
	else
	{
		return cx;
	}
};

/*
| Transforms a string move by
| this set actually happening first.
*/
def.proto._transformStringMove =
	function( cx )
{
	// TODO this is not really correct
	return cx;
};
