/*
| Adds moves/adds and removes an entry to a twig.
*/
throw new Error( 'XXX' );

def.extend = 'Change/Base';

def.attributes =
{
	// rank of new node
	rankFrom: { type: [ 'undefined', 'integer' ], json: true },

	// rank of new node
	rankTo: { type: [ 'undefined', 'integer' ], json: true },

	// move from this trace
	traceFrom: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// move to this trace
	traceTo: { type: [ 'undefined', 'ti2c:Trace' ], json: true },

	// value to set
	val: { type: [ '< Change/Vals' ], json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeError    } from '{Change/Error}';
import { Self as Sequence       } from '{Change/Sequence}';
import { Self as Simulcast      } from '{Change/Simulcast}';
import { Self as Trace          } from '{ti2c:Trace}';
import { Self as TreeAssign     } from '{Change/Tree/Assign}';
import { Self as TreeShrink     } from '{Change/Tree/Shrink}';

/*
| Performs the growth change on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	const from = this.traceFrom;
	const to   = this.traceTo;
	const lastFrom = from?.last;
	const lastTo   = to?.last;

	if( from )
	{
		const atFrom = lastFrom.at;
		const name = lastFrom.name;

		let pivot = from.back.pick( tree );
		let twig = pivot[ name ];
	}

	const tback = t.back;
	const tlast = t.last;
	let pivot = tback.pick( tree );
	let twig = pivot[ tlast.name ];


	const key = tlast.key;
	const rank = this.rank;
	if( rank > twig.length ) throw ChangeError.make( 'grow.rank > pivot.length' );
	twig = twig.create( 'twig:insert', key, rank, this.val );
	pivot = pivot.create( tlast.name, twig );
	return tback.graft( tree, pivot );
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	return this.create( 'trace', this.trace?.chop );
};

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );
	if( json.$type !== this.$type ) throw new Error( );

	const trace = Trace.FromJson( json.trace, plan );
	const rank = json.rank;
	const val = trace.last.fromJsonVar( json, plan, 'val' );
	return Self.create( 'trace', trace, 'rank', rank, 'val', val );
};

/*
| This change reversed.
*/
def.lazy.reversed =
	function( )
{
	const inv = TreeShrink.create( 'trace', this.trace, 'prev', this.val, 'rank', this.rank );
	ti2c.aheadValue( inv, 'reversed', this );
	return inv;
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
	const rankFrom  = this.rankFrom;
	const rankTo    = this.rankTo;
	const traceFrom = this.traceFrom;
	const traceTo   = this.traceTo;

	if( rankFrom !== undefined )
	{
		if( rankFrom < 0 || !traceFrom )
		{
			throw ChangeError.make( 'invalid TreeMove' );
		}
	}
	else
	{
		if( traceFrom )
		{
			throw ChangeError.make( 'invalid TreeMove' );
		}
	}

	if( rankTo !== undefined )
	{
		if( rankTo < 0 || !traceTo )
		{
			throw ChangeError.make( 'invalid TreeMove' );
		}
	}
	else
	{
		if( traceTo )
		{
			throw ChangeError.make( 'invalid TreeMove' );
		}
	}
};

/*
| Transforms a shrink by this shrink.
*/
def.proto._transformGrowShrink =
	function( c )
{
	if( this.trace.back !== c.trace.back ) return c;
	if( this.rank > c.rank ) return c;
	return c.create( 'rank', c.rank + 1 );
};

/*
| Maps transformables to transform functions
*/
def.staticLazy._transformers =
	( ) =>
{
	const map = new Map( );
	map.set( Self,           '_transformGrowShrink'     );
	map.set( Sequence,       '_transformSequence'       );
	map.set( Simulcast,      '_transformSimulcast'      );
	map.set( TreeAssign,     '_transformSame'           );
	map.set( TreeShrink,     '_transformGrowShrink'     );
	map.set( Trace,          '_transformSame'           );
	return map;
};
