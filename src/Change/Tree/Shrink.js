/*
| Removes a tree node.
*/
def.extend = 'Change/Base';

def.attributes =
{
	// shrinks at this trace
	trace: { type: 'ti2c:Trace', json: true },

	// value the tree had
	// FIXME rename val
	prev: { type: [ '< Change/Vals' ], json: true },

	// rank of removed node
	rank: { type: 'integer', json: true },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ChangeError      } from '{Change/Error}';
import { Self as ChangeListMove   } from '{Change/List/Move}';
import { Self as ChangeTreeAssign } from '{Change/Tree/Assign}';
import { Self as ChangeTreeGrow   } from '{Change/Tree/Grow}';
import { Self as Sequence         } from '{Change/Sequence}';
import { Self as Simulcast        } from '{Change/Simulcast}';
import { Self as Trace            } from '{ti2c:Trace}';

/*
| Performs the shrinking change on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	// Stores the old value for history tracking.
	const prev = this.trace.pick( tree );

	if( prev !== this.prev && !( prev.jsonfy( ) && prev.jsonfy( ) === this.prev.jsonfy( ) ) )
	{
		throw ChangeError.make( 'shrink.prev doesn\'t match' );
	}

	const t = this.trace;
	const tback = t.back;
	const tlast = t.last;
	let pivot = tback.pick( tree );
	let twig = pivot[ tlast.name ];
	const key = tlast.key;
	const rank = twig.rankOf( key );
	if( rank !== this.rank ) throw ChangeError.make( 'shrink.rank doesn\'t match' );
	twig = twig.create( 'twig:remove', key );
	pivot = pivot.create( tlast.name, twig );
	return tback.graft( tree, pivot );
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	return this.create( 'trace', this.trace?.chop );
};

/*
| Custom from JSON creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );
	if( json.$type !== this.$type ) throw new Error( );
	const rank = json.rank;
	const trace = Trace.FromJson( json.trace, plan );
	const prev = trace.last.fromJsonVar( json, plan, 'prev' );
	return Self.create( 'prev', prev, 'rank', rank, 'trace', trace );
};

/*
| Returns the inversion to this change.
*/
def.lazy.reversed =
	function( )
{
	const inv = ChangeTreeGrow.create( 'trace', this.trace, 'val', this.prev, 'rank', this.rank );
	ti2c.aheadValue( inv, 'reversed', this );
	return inv;
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
	if( this.rank < 0 ) throw ChangeError.make( 'three.shrink.rank negative' );
	if( this.prev === undefined ) throw ChangeError.make( 'tree.shrink.prev undefined' );
};

/*
| Maps transformables to transform functions
*/
def.staticLazy._transformers =
	( ) =>
{
	const map = new Map( );
	map.set( Self,             '_transformGrowShrink'     );
	map.set( ChangeListMove,   '_transformListMove'       );
	map.set( ChangeTreeGrow,   '_transformGrowShrink'     );
	map.set( ChangeTreeAssign, '_transformTreeAsign'      );
	map.set( Sequence,         '_transformSequence'       );
	map.set( Simulcast,        '_transformSimulcast'      );
	map.set( Trace,            '_transformTrace'          );
	return map;
};

/*
| Transforms a grow or shrink by this shrink.
*/
def.proto._transformGrowShrink =
	function( c )
{
	if( this.trace.back !== c.trace.back ) return c;

	const cRank = c.rank;
	const tRank = this.rank;

	if( tRank > cRank ) return c;
	if( tRank === cRank ) return undefined;
	return c.create( 'rank', cRank - 1 );
};

/*
| Transforms a list move.
*/
def.proto._transformListMove =
	function( c )
{
	if( this.trace.hasTrace( c.traceFrom ) )
	{
		return undefined;
	}
	else
	{
		return c;
	}
};

/*
| Transforms a insert/remove/set/split changes
| by this shrink.
*/
def.proto._transformTreeAssign =
	function( cx )
{
	if( !this.trace.hasTrace( cx.trace ) )
	{
		return cx;
	}
	else
	{
		// otherwise it is gone!
		return undefined;
	}
};

/*
| Transforms an offset by this set.
*/
def.proto._transformOffset =
	function( offset )
{
/**/if( CHECK && offset.name !== 'offset' ) throw new Error( );

	// is the offset trace on another paragraph?
	// since the offset stores para key there is no change
	// needed even it is below the split
	if( this.trace !== offset.backward( 'items' ) )
	{
		return offset;
	}
	else
	{
		return undefined;
	}
};

/*
| Transforms a trace by this set.
*/
def.proto._transformTrace =
	function( trace )
{
	// TODO make it more flexible than just items
	if( this.trace !== trace.backward( 'items' ) )
	{
		return trace;
	}
	else
	{
		return undefined;
	}
};
