/*
| A list of changes to be applied sequentially.
*/
def.extend = 'list@<Change/Types';
def.json = true;

import { Self as SetString      } from '{set@string}';
import { Self as Simulcast      } from '{Change/Simulcast}';

/*
| The set of twig keys affected by this change list.
*/
def.lazy.affectedTwigItems =
	function( )
{
	const affected = new Set( );
	for( let change of this )
	{
		const trace = change.trace?.backward( 'items' );
		if( trace ) affected.add( trace.last.key );

		const traceFrom = change.traceFrom?.backward( 'items' );
		if( traceFrom ) affected.add( traceFrom.last.key );

		const traceTo = change.traceTo?.backward( 'items' );
		if( traceTo ) affected.add( traceTo.last.key );
	}
	return SetString.create( 'set:init', affected );
};

/*
| Turns the change into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	let str = 'sequence [\n';
	for( let change of this )
	{
		str += change.asString;
	}
	str += ']\n';
	return str;
};

/*
| Performes this change list on a tree.
*/
def.proto.changeTree =
	function( tree )
{
	for( let change of this )
	{
		// the tree returned by op-handler is the new tree
		tree = change.changeTree( tree );
	}
	return tree;
};

/*
| Removes the one entry from the front of the traces.
*/
def.lazy.chop =
	function( )
{
	const list = [ ];
	for( let c of this )
	{
		list.push( c.chop );
	}
	return this.create( 'list:init', list );
};

/*
| If it is already a sequence, this is a noop.
*/
def.lazy.sequence =
	function( )
{
	return this;
};

/*
| Transforms the list such that all changes are
| respective to the beginning state.
*/
def.lazy.simulcast =
	function( )
{
	let list = this.clone( );
	const len = this.length;

	for( let a = len - 1; a >= 0; a-- )
	{
		const ca = this.get( a ).reversed;
		for( let b = a + 1; b < len; b++ )
		{
			const cb = list[ b ];
			if( !cb ) continue;
			list[ b ] = ca.transform( cb );
		}
	}

	//list = list.filter( ( e ) => e !== undefined );
	for( let a = 0, alen = list.length; a < alen; a++ )
	{
		let e = list[ a ];

		if( e === undefined )
		{
			list.splice( a, 1 );
			alen--;
			continue;
		}

		if( e.ti2ctype === Self )
		{
			e = e.simulcast;
		}

		if( e.ti2ctype === Simulcast )
		{
			const l2 = e.clone( );
			list.splice( a, 1, ...l2 );
			const skip = l2.length - 1;
			a += skip;
			alen += skip;
		}
	}

	return Simulcast.Array( list );
};

/*
| Returns a change list with reversed changes.
*/
def.lazy.reversed =
	function( )
{
	const list = [ ];
	for( let c of this.reverse( ) )
	{
		list.push( c.reversed );
	}
	const result = Self.Array( list );
	ti2c.aheadValue( result, 'reversed', this );
	return result;
};

/*
| Returns the result of a change transformed by this.
*/
def.proto.transform =
	function( other )
{
	if( !other )
	{
		return other;
	}

	switch( other.ti2ctype )
	{
		case Self:
			return this._transformSequence( other );

		default:
			return this._transformSingle( other );
	}
};

/*
| Returns a Sequence
| transformed by this Sequence as if it
| actually came first.
|
| ~return: a Change or a Simulcast.
*/
def.proto._transformSequence =
	function( seq )
{
	let sim = seq.simulcast;
	for( let ch of this )
	{
		sim = ch.transform( sim );
	}
	return sim;
};

/*
| Returns the result of a change
| transformed by this ChangeList as if it
| actually came first.
|
| ~return: a Change or a Simulcast.
*/
def.proto._transformSingle =
	function( cx )
{
	for( let ch of this )
	{
		cx = ch.transform( cx );
	}
	return cx;
};
