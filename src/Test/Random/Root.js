/*
| The root of the test
*/
def.abstract = true;

import timers     from 'node:timers/promises';
import seedrandom from 'seedrandom';

import { Self as Dynamic    } from '{Test/Dynamic}';
import { Self as FabricPara } from '{Test/Fabric/Para}';
import { Self as Helper     } from '{Test/Helper}';
import { Self as ListMove   } from '{Change/List/Move}';
import { Self as Simulcast  } from '{Change/Simulcast}';
import { Self as StringMove } from '{Change/String/Move}';
import { Self as TraceRoot  } from '{Test/Trace/Root}';

/*
| Minimum and maximum val length of operations.
*/
const _maxLen = 100;

/*
| Verbose level.
*/
let _verbose;

/*
| Global success counter for 'minimize'.
*/
let _N;

/*
| List of changes to create.
*/
const chancesChanges =
[
	{ chance: 20, otFunc: rndChgStringInsert },
	{ chance: 20, otFunc: rndChgStringRemove },
	{ chance: 20, otFunc: rndChgStringMove   },
//	{ chance:  3, otFunc: rndChgParaInsert   },
//	{ chance:  3, otFunc: rndChgParaRemove   },
	{ chance: 30, otFunc: rndChgParaInsert   },
	{ chance: 30, otFunc: rndChgParaRemove   },
];

/*
| Randomly creates op at much this distance in past.
*/
const chancesPast =
[
	{ distance: 0, chance: 10 },
	{ distance: 1, chance:  5 },
	{ distance: 2, chance:  5 },
];

/*
| A "die" with as many sides as total chances.
*/
let dieChgs, diePast;

/*
| Shortcut from Helper.
*/
let logChg;

/*
| Re-seeds the random generator.
*/
let prng;
function randomize( seed )
{
	if( seed === undefined )
	{
		seed = '';
		for( let a = 0; a < 10; a++ )
		{
			seed += randChars.charAt( Math.floor( Math.random( ) * randChars.length ) );
		}
	}

	console.log( 'seed', seed );
	prng = seedrandom( seed );
	return seed;
}

const randChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

/*
| Returns a random number from 0 to max.
*/
function rand( min, max )
{
	return Math.floor( prng( ) * ( max + 1  - min ) ) + min;
}

/*
| Returns a random string of at least min at most max length.
*/
function randStr( min, max )
{
	const len = rand( min, max );
	let result = '';
	for( let a = 0; a < len; a++ )
	{
		result += randChars.charAt( rand( 0, randChars.length - 1 ) );
	}
	return result;
}

/*
| Creates a random para insertion.
*/
function rndChgParaInsert( dynamic, at )
{
	//console.log( 'insert para' );
	const text = dynamic.treeAt( at - 1 );
	const iLine = rand( 0, text.paras.length );

	return(
		ListMove.create(
			'traceTo',
				TraceRoot.text
				.add( 'paras', iLine ),
			'val',
				FabricPara.create( 'string', randStr( 1, _maxLen ) ),
		)
	);
}

/*
| Creates a random para removal.
*/
function rndChgParaRemove( dynamic, at )
{
	const text = dynamic.treeAt( at - 1 );
	if( text.paras.length === 0 ) return undefined;
	const iLine = rand( 0, text.paras.length - 1 );

	return(
		ListMove.create(
			'traceFrom',
				TraceRoot.text
				.add( 'paras', iLine ),
			'val',
				text.paras.get( iLine ),
		)
	);
}

/*
| Creates a random string insertion.
*/
function rndChgStringInsert( dynamic, at )
{
	//console.log( 'insert string' );
	const text = dynamic.treeAt( at - 1 );
	if( text.paras.length === 0 ) return undefined;
	const iLine = rand( 0, text.paras.length - 1 );
	const para = text.paras.get( iLine );
	const iOffset = rand( 0, para.string.length );
	return(
		StringMove.create(
			'traceFrom',
				undefined,
			'traceTo',
				TraceRoot.text
				.add( 'paras', iLine )
				.add( 'string' )
				.add( 'offset', iOffset ),
			'val',
				randStr( 1, _maxLen ),
		)
	);
}

/*
| Creates a random string removal.
*/
function rndChgStringRemove( dynamic, at )
{
	const text = dynamic.treeAt( at - 1 );
	if( text.paras.length === 0 ) return undefined;

	const iLine = rand( 0, text.paras.length - 1 );
	const para = text.paras.get( iLine );
	const iOffset = rand( 0, para.string.length - 1 );
	const string = para.string;
	if( string === '' ) return undefined;
	const iLen = rand( 1, Math.min( _maxLen, para.string.length - iOffset ) );

	return(
		StringMove.create(
			'traceFrom',
				TraceRoot.text
				.add( 'paras', iLine )
				.add( 'string' )
				.add( 'offset', iOffset ),
			'traceTo',
				undefined,
			'val',
				string.substr( iOffset, iLen ),
		)
	);
}

/*
| Creates a random string move.
*/
function rndChgStringMove( dynamic, at )
{
	const text = dynamic.treeAt( at - 1 );
	if( text.paras.length === 0 ) return undefined;

	const iLineFrom = rand( 0, text.paras.length - 1 );
	const paraFrom = text.paras.get( iLineFrom );
	const iOffsetFrom = rand( 0, paraFrom.string.length - 1 );
	const stringFrom = paraFrom.string;
	if( stringFrom === '' ) return undefined;

	const iLen = rand( 0, Math.min( _maxLen, paraFrom.string.length - iOffsetFrom ) );

	const iLineTo = rand( 0, text.paras.length - 1 );
	const paraTo = text.paras.get( iLineTo );
	const iOffsetTo = rand( 0, paraTo.string.length );

	if(
		iLineFrom === iLineTo
		&& iOffsetTo > iOffsetFrom
		&& iOffsetTo <= iOffsetFrom + iLen
	)
	{
		// skips invalid move rolls
		return undefined;
	}

	return(
		StringMove.create(
			'traceFrom',
				TraceRoot.text
				.add( 'paras', iLineFrom )
				.add( 'string' )
				.add( 'offset', iOffsetFrom ),
			'traceTo',
				TraceRoot.text
			.add( 'paras', iLineTo )
				.add( 'string' )
				.add( 'offset', iOffsetTo ),
			'val',
				stringFrom.substr( iOffsetFrom, iLen ),
		)
	);
}

/*
| Builds a random dynamic starting with 'dynamic'
| of 'n' changes.
*/
function buildRandomDynamic( dynamic, n )
{
	for( let i = 0; i < n; i++ )
	{
		if( _verbose >= 5 )
		{
			console.log( '--------' );
			console.log( 'history:' );
			for( let at = 0, atlen = dynamic.length; at < atlen; at++ )
			{
				console.log( '@ ' + at + ':' );
				dynamic.log( at );
			}
			console.log( '--------' );
		}
		_N = i;
		const sideChgs = dieChgs[ rand( 0, dieChgs.length - 1 ) ];
		const sidePast = diePast[ rand( 0, diePast.length - 1 ) ];
		let distance = sidePast.distance;

		if( distance > dynamic.length - 1 ) distance = 0;
		const at = dynamic.length - distance;
		if( _verbose >= 3 )
		{
			console.log( 'adding @ ' + ( at - 1 ) + ' (' + ( dynamic.length - 1 ) + ')' );
		}
		let chgx = sideChgs.otFunc( dynamic, at );
		if( !chgx || ( chgx.ti2ctype === StringMove && chgx.isNoop ) )
		{
			if( _verbose >= 3 ) console.log( 'not a suitable random change' );
			continue;
		}
		if( chgx.ti2ctype === StringMove ) chgx.check( );

		if( _verbose >= 3 ) logChg( chgx );

		if( distance > 0 )
		{
			for( let d = dynamic.length - distance, dlen = dynamic.length; d < dlen; d++ )
			{
				let chgd = dynamic.get( d );

				if( _verbose >= 5 ) logChg( chgd, 'transforming @ distance: ' + d );

				/*
				if( chgd.ti2ctype === StringMove && chgd.val.length > 1 )
				{
					chgd = chgd.explodeSequence;
					if( _verbose >= 5 ) logChg( chgd, 'chgd.explode' );
				}

				if( chgx.ti2ctype === StringMove && chgx.val.length > 1 )
				{
					chgx = chgx.explodeSimulcast;
					if( _verbose >= 5 ) logChg( chgx, 'chgx.explode' );
				}
				*/

				chgx = chgd.transform( chgx );
				if( chgx === undefined )
				{
					if( _verbose >= 5 ) console.log( 'transformed to undefined!' );
					break;
				}

				if( _verbose >= 5 ) logChg( chgx, 'step result' );
			}

			if( chgx === undefined ) continue;
		}
		if( chgx.ti2ctype === Simulcast ) chgx = chgx.sequence;

		dynamic = dynamic.append( chgx );
	}

	return dynamic;
}

/*
| Tests that reversing all changes of a dynamic should create
| the start texting again.
*/
function testReverse( dynamic )
{
	// does not undo the change at pos 0 which is init set.
	for( let a = dynamic.length - 1; a > 0; a-- )
	{
		dynamic = dynamic.append( dynamic.get( a ).reversed );
	}

	if( dynamic.tree !== dynamic.treeAt( 0 ) )
	{
		if( _verbose >= 1 )
		{
			console.log( '!! testReverse failed !!' );
			console.log( 'start tree', dynamic.treeAt( 0 ).jsonfy( ' ' ) );
			console.log( 'reverted tree', dynamic.tree.jsonfy( ' ' ) );
		}
		throw new Error( );
	}
}

/*
| Tests that exploding all changes of a dynamic should create
| the same result.
*/
function testExplode( dynamic )
{
	let dynamicExp = dynamic.shorten( 1 );

	for( let a = 1, alen = dynamic.length; a < alen; a++ )
	{
		let chg = dynamic.get( a );
		if( chg.ti2ctype === StringMove ) chg = chg.explodeSequence;
		dynamicExp = dynamicExp.append( chg );
	}

	if( dynamic.tree !== dynamicExp.tree )
	{
		if( _verbose >= 1 )
		{
			console.log( '!! testExplode failed !!' );
			console.log( 'plain tree', dynamic.treeA.jsonfy( ' ' ) );
			console.log( 'exploded tree', dynamic.tree.jsonfy( ' ' ) );
		}
		throw new Error( );
	}
}

/*
| Tests that exploding all changes of a dynamic as Simulcast and
| then converting them to sequence should still create the same result.
*/
function testExplodeSimulcast( dynamic )
{
	let dynamicExp = dynamic.shorten( 1 );

	for( let a = 1, alen = dynamic.length; a < alen; a++ )
	{
		let chg = dynamic.get( a );
		if( chg.ti2ctype === StringMove )
		{
			chg = chg.explodeSimulcast;
			if( chg.ti2ctype === Simulcast ) chg = chg.sequence;
		}
		dynamicExp = dynamicExp.append( chg );
	}

	if( dynamic.tree !== dynamicExp.tree )
	{
		if( _verbose >= 1 )
		{
			console.log( '!! testExplode failed !!' );
			console.log( 'plain tree', dynamic.treeA.jsonfy( ' ' ) );
			console.log( 'exploded tree', dynamic.tree.jsonfy( ' ' ) );
		}
		throw new Error( );
	}
}

/*
| Runs a test series once with n changes.
|
| Sets the global _N to the length of changes gotten
| (until an exception might abrupt this)
*/
function runOnce( n )
{
	const dynamicStart =
		Dynamic.InitText(
//			'Sphinx-of.black+quartz,!judge?my§vow',
//			'How_quickly"daft:jumping/zebras#vex!',
			'Sphinx-of',
			'How_quickly',
		);

	let dynamic = buildRandomDynamic( dynamicStart, n );
	testReverse( dynamic );
	testExplode( dynamic );
	testExplodeSimulcast( dynamic );

		/*
		if( chgx.val.length > 1 )
		{
			// XXX
			if( chgx.explodeSequence.simulcast !== chgx.explodeSimulcast )
			{
				console.log( 'OJE1!' );
				logChg( chgx, 'chg' );
				logChg( chgx.explodeSequence, 'explodeSequence', );
				logChg( chgx.explodeSequence.simulcast, 'explodeSequence.simulcast' );
				logChg( chgx.explodeSimulcast, 'explodeSimulcast' );
				throw new Error( );
			}

			if( chgx.explodeSimulcast.sequence !== chgx.explodeSequence )
			{
				console.log( 'OJE2!' );
				logChg( chgx, 'chg' );
				logChg( chgx.explodeSequence, 'explodeSequence', );
				logChg( chgx.explodeSequence.simulcast, 'explodeSequence.simulcast' );
				logChg( chgx.explodeSimulcast, 'explodeSimulcast' );
				throw new Error( );
			}
		}
		*/

	const textFin = dynamic.tree;
	if( _verbose >= 1 )
	{
		console.log( textFin.jsonfy( ' ' ) );
	}
}

/*
| Runs the random test.
|
| ~minimize: if true tries to find a minimal case that errors (when a known failure exists).
| ~seed:     if defined forces this seed for random (for debugging known failures)
| ~verbose:  verbose level from 0 to 5
| ~n:        test this amount of changes
*/
async function run( minimize, seed, verbose, n )
{
	console.log( 'Random tests, verbosity:', verbose, 'n:', n );
	_verbose = verbose;

	// builds the dice
	dieChgs = [ ];
	for( let a = 0, alen = chancesChanges.length; a < alen; a++ )
	{
		const ch = chancesChanges[ a ];
		for( let b = 0; b < ch.chance; b ++ ) dieChgs.push( ch );
	}

	diePast = [ ];
	for( let a = 0, alen = chancesPast.length; a < alen; a++ )
	{
		const ch = chancesPast[ a ];
		for( let b = 0; b < ch.chance; b ++ ) diePast.push( ch );
	}

	if( minimize )
	{
		let shortestN = n + 1;
		let shortestSeed;
		for( ;; )
		{
			_N = 0;
			let seed;
			try
			{
				seed = randomize( );
				runOnce( shortestN - 1 );
			}
			catch( e )
			{
				if( _verbose >= 1 ) console.log( 'CATCHING', _N, e );

				if( _N < shortestN )
				{
					shortestN = _N;
					shortestSeed = seed;
					console.log( '******************************************' );
					console.log( shortestN, shortestSeed );
					console.log( '******************************************' );
					if( _N <= 2 ) break;
				}
			}

			// allows garbage collector to run
			await timers.setTimeout( 1 );
		}
	}
	else if( seed !== undefined )
	{
		randomize( seed );
		runOnce( n );
	}
	else
	{
		for( let a = 0; a < 100; a++ )
		{
			randomize( );
			runOnce( n );
		}
	}
}

/*
| Runs the tests.
*/
def.static.init =
	async function( )
{
	logChg = Helper.logChg;
	const argv = process.argv;

	let minimize;
	let seed;
	let verbose = 0;
	let n = 25;

	for( let a = 2; a < argv.length; a++ )
	{
		const arg = argv[ a ];
		if( arg === 'minimize' )
		{
			minimize = true;
		}
		else if( arg.startsWith( 'n=' ) )
		{
			n = parseInt( arg.substring( 2 ), 10 );
		}
		else if( arg.startsWith( 'seed=' ) )
		{
			seed = arg.substring( 5 );
		}
		else if( arg.startsWith( 'verbose=' ) )
		{
			verbose = parseInt( arg.substring( 8 ), 10 );
		}
		else
		{
			console.log( 'argument invalid: ', arg );
		}
	}

	await run( minimize, seed, verbose, n );
};
