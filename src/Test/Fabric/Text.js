/*
| A sequence of paragraphs.
*/
def.attributes =
{
	// the paragraphs
	paras: { type: 'list@Test/Fabric/Para', json: true },
};

def.json = true;
