/*
| A paragraph.
*/
def.attributes =
{
	// the paragraphs string
	string: { type: 'string', json: true },
};

def.json = true;
