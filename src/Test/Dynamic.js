/*
| The changes dynamic.
*/
def.attributes =
{
	// history of all changes as sequence
	_sequence: { type: 'Change/Sequence' },
};

import { Self as FabricPara     } from '{Test/Fabric/Para}';
import { Self as FabricText     } from '{Test/Fabric/Text}';
import { Self as ListFabricPara } from '{list@Test/Fabric/Para}';
import { Self as Sequence       } from '{Change/Sequence}';
import { Self as Simulcast      } from '{Change/Simulcast}';
import { Self as TraceRoot      } from '{Test/Trace/Root}';
import { Self as TreeAssign     } from '{Change/Tree/Assign}';

/*
| Helper to append, appends recursive change(lists)
| as flat list to a single list.
*/
function _appendRecursive( seq, other )
{
/**/if( CHECK )
/**/{
/**/	if( !Array.isArray( other ) && other.ti2ctype !== Sequence ) throw new Error( );
/**/}

	for( let e of other )
	{
		switch( e.ti2ctype )
		{
			case Sequence:
				seq = _appendRecursive( seq, e );
				break;

			case Simulcast:
				seq = _appendRecursive( seq, e.sequence );
				break;

			default:
				seq = seq.append( e );
				break;
		}
	}
	return seq;
}

/*
| Appends one or several changes.
*/
def.proto.append =
	function( ...changes )
{
	return this.create( '_sequence', _appendRecursive( this._sequence, changes ) );
};

/*
| Asserts the context to be a text of given lines.
*/
def.proto.assertText =
	function( ...lines )
{
	const text = this.tree;
	const paras = text.paras;

	if( text.ti2ctype !== FabricText ) throw new Error( );

	const plen = paras.length;
	let fine = true;

	if( lines.length !== plen ) fine = false;

	if( fine )
	{
		for( let a = 0; a < plen; a++ )
		{
			const para = paras.get( a );
			if( para.string !== lines[ a ] )
			{
				fine = false;
				break;
			}
		}
	}

	if( !fine )
	{
		console.log( 'assertTree failed' );
		console.log( 'expected:' );
		for( let a = 0; a < plen; a++ )
		{
			console.log( '  [' + a + ']: >' + lines[ a ] + '<' );
		}
		console.log( 'but got:' );
		for( let a = 0; a < plen; a++ )
		{
			console.log( '  [' + a + ']: >' + paras.get( a ).string + '<' );
		}
		throw new Error( );
	}
};

/*
| Returns the change at 'pos'.
*/
def.proto.get =
	function( pos )
{
	return this._sequence.get( pos );
};

/*
| Initializes the dynamic with a Text.
*/
def.static.InitText =
	function( ...lines )
{
	const paras = [ ];
	for( let line of lines )
	{
		paras.push( FabricPara.create( 'string', line ) );
	}
	const fText = FabricText.create( 'paras', ListFabricPara.Array( paras ) );
	const set =
		TreeAssign.create(
			'prev', undefined,
			'trace', TraceRoot.text,
			'val', fText,
		);
	return Self.create( '_sequence', Sequence.Elements( set ) );
};

/*
| The dynamic length.
*/
def.lazy.length =
	function( )
{
	return this._sequence.length;
};

/*
| Prints the tree on console.log
|
| ~at: change position to log it at.
*/
def.proto.log =
	function( at )
{
	console.log( 'text:' );
	if( at === undefined ) at = this.length - 1;
	const text = this.treeAt( at );
	const paras = text.paras;
	let maxlen = 0;
	for( let a = 0, alen = paras.length; a < alen; a++ )
	{
		const plen = paras.get( a ).string.length;
		if( plen > maxlen ) maxlen = plen;
	}

	{
		let s = '';
		for( let a = 0; a < maxlen; a++ )
		{
			s += Math.floor( a / 10 ) % 10;
		}
		console.log( '  %%%%% ' + s );
	}
	{
		let s = '';
		for( let a = 0; a < maxlen; a++ )
		{
			s += a % 10;
		}
		console.log( '  %%%%% ' + s );
	}

	for( let a = 0, alen = paras.length; a < alen; a++ )
	{
		console.log( '  [' + a + ']: >' + paras.get( a ).string + '<' );
	}
};

/*
| Shortens the amount of changes in this dynamic.
*/
def.proto.shorten =
	function( len )
{
	return this.create( '_sequence', this._sequence.slice( 0, len ) );
};

/*
| Returns the tree at the end of the changes.
*/
def.lazy.tree =
	function( pos )
{
	return this.treeAt( this.length - 1 );
};

/*
| Returns the tree at 'pos'.
*/
def.proto.treeAt =
	function( pos )
{
	const seq = this._sequence;

	if( pos < 0 || pos > seq.length )
	{
		throw new Error( );
	}

	let tree;
	for( let a = 0; a <= pos; a++ )
	{
		tree = seq.get( a ).changeTree( tree );
	}
	return tree;
};

