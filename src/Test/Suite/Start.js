/*
| Starts self testing.
*/
import util from 'util';
util.inspect.defaultOptions.depth = null;

global.CHECK = true;

await import( 'ti2c' );
const pkg =
	await ti2c.register(
		'name',     'ot-test',
		'meta',     import.meta,
		'source',  'src/',
		'relPath', 'Test/Suite/Start',
		'codegen', 'codegen/',
	);
const Root = await pkg.import( 'Test/Suite/Root' );
Root.init( pkg.rootDir );
