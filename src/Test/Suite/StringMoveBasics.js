/*
| Tests basic string moves.
*/
def.abstract = true;

import { Self as Dynamic } from '{Test/Dynamic}';
import { Self as Helper  } from '{Test/Helper}';

let chgStringMove;

/*
| Tests inserting a string.
*/
function stringInsert( )
{
	console.log( '  * StringInsert' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'to', 0, 10,
			'val', 'VERY '
		);
	dynamic.append( chg1 )
	.assertText(
		'Sphinx of VERY black quartz, judge my vow',
	);
}

/*
| Tests moving a string to another para
*/
function stringMoveAcross( )
{
	console.log( '  * StringMoveAcross' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'to',   1, 25,
			'val', 'black '
		);
	dynamic.append( chg1 )
	.assertText(
		'Sphinx of quartz, judge my vow',
		'How quickly daft jumping black zebras vex!',
	);
}

/*
| Tests moving a string backward.
*/
function stringMoveBackward( )
{
	console.log( '  * StringMoveBackward' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 30,
			'to',   0, 10,
			'val', 'my '
		);
	dynamic.append( chg1 )
	.assertText(
		'Sphinx of my black quartz, judge vow',
	);
}

/*
| Tests moving a string forward.
*/
function stringMoveForward( )
{
	console.log( '  * StringMoveForward' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'to',   0, 33,
			'val', 'black '
		);
	dynamic.append( chg1 )
	.assertText( 'Sphinx of quartz, judge my black vow' );
}

/*
| Runs the test.
*/
def.static.run =
	function( )
{
	console.log( '* StringMoveBasics' );
	chgStringMove = Helper.chgStringMove;

	stringInsert( );
	stringMoveAcross( );
	stringMoveBackward( );
	stringMoveForward( );
};
