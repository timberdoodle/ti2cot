/*
| Tests text combinations.
*/
def.abstract = true;

import { Self as Dynamic    } from '{Test/Dynamic}';
import { Self as FabricPara } from '{Test/Fabric/Para}';
//import { Self as Helper     } from '{Test/Helper}';
import { Self as ListMove   } from '{Change/List/Move}';
import { Self as Sequence   } from '{Change/Sequence}';
import { Self as StringMove } from '{Change/String/Move}';
import { Self as TraceRoot  } from '{Test/Trace/Root}';

//let logChg;

/*
| Tests transform a join on a split
*/
def.static.testTransformJoinOnSplit =
	function( )
{
	console.log( '  * TransformJoinOnSplit' );

	const dynamic = Dynamic.InitText( 'Ameno', 'Latire', 'Dorime' );
	const tracePara1 = TraceRoot.text.add( 'paras', 1 );
	const tracePara2 = TraceRoot.text.add( 'paras', 2 );

	const chg1 =
		Sequence.Elements(
			ListMove.create(
				'traceTo', tracePara2,
				'val', FabricPara.create( 'string', '' )
			),
			StringMove.create(
				'traceFrom', tracePara1.add( 'string' ).add( 'offset', 2 ),
				'traceTo',   tracePara2.add( 'string' ).add( 'offset', 0 ),
				'val', 'tire',
			),
			StringMove.create(
				'traceFrom', tracePara1.add( 'string' ).add( 'offset', 2 ),
				'traceTo',   tracePara2.add( 'string' ).add( 'offset', 4 ),
				'val', '',
			)
		);

	dynamic.append( chg1 )
	.assertText(
		'Ameno',
		'La',
		'tire',
		'Dorime',
	);
	const chg2 =
		Sequence.Elements(
			StringMove.create(
				'traceFrom', tracePara2.add( 'string' ).add( 'offset', 0 ),
				'traceTo',   tracePara1.add( 'string' ).add( 'offset', 6 ),
				'val', 'Dorime',
			),
			ListMove.create(
				'traceFrom', tracePara2,
				'val', FabricPara.create( 'string', '' )
			),
		);

	dynamic.append( chg2 )
	.assertText(
		'Ameno',
		'LatireDorime',
	);

	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T )
	.assertText(
		'Ameno',
		'LaDorime',
		'tire',
	);
};

/*
| Runs the test.
*/
def.static.run =
	function( )
{
	console.log( '* TextCombos' );
	//logChg = Helper.logChg;
	Self.testTransformJoinOnSplit( );
};
