/*
| Tests transformations by list insert/remove.
*/
def.abstract = true;

import { Self as ChangeListMove   } from '{Change/List/Move}';
import { Self as ChangeStringMove } from '{Change/String/Move}';
import { Self as FabricPara       } from '{Test/Fabric/Para}';
import { Self as Dynamic          } from '{Test/Dynamic}';
import { Self as TraceRoot        } from '{Test/Trace/Root}';

/*
| Tests transformation of a list insertion on a list insertion.
*/
function transform_listInsert_listInsert( )
{
	console.log( '  * transform_listInsert_listInsert' );

	const dynamic =
		Dynamic.InitText(
			'abcdef',
			'123',
		);
	const tracePara1 = TraceRoot.text.add( 'paras',  1 );
	const tracePara2 = TraceRoot.text.add( 'paras',  2 );

	const chg1 =
		ChangeListMove.create(
			'traceTo', tracePara1,
			'val',     FabricPara.create( 'string', 'ABC' )
		);

	dynamic.append( chg1 )
	.assertText(
		'abcdef',
		'ABC',
		'123',
	);

	const chg2 =
		ChangeListMove.create(
			'traceTo', tracePara2,
			'val', FabricPara.create( 'string', 'DEF' )
		);

	dynamic.append( chg2 )
	.assertText(
		'abcdef',
		'123',
		'DEF',
	);

	const chg2T = chg1.transform( chg2 );

	dynamic.append( chg1, chg2T )
	.assertText(
		'abcdef',
		'ABC',
		'123',
		'DEF',
	);
}

/*
| Tests transformation of a list insertion on a list removal.
*/
function transform_listInsert_listRemove( )
{
	console.log( '  * transform_listInsert_listRemove' );

	const dynamic = Dynamic.InitText( 'abcdef', '123', 'ABC' );
	const tracePara1 = TraceRoot.text.add( 'paras',  1 );
	const tracePara2 = TraceRoot.text.add( 'paras',  2 );

	const chg1 =
		ChangeListMove.create(
			'traceFrom', tracePara1,
			'val', FabricPara.create( 'string', '123' )
		);

	dynamic.append( chg1 )
	.assertText(
		'abcdef',
		'ABC',
	);

	const chg2 =
		ChangeListMove.create(
			'traceTo', tracePara2,
			'val', FabricPara.create( 'string', 'DEF' )
		);

	dynamic.append( chg2 )
	.assertText(
		'abcdef',
		'123',
		'DEF',
		'ABC',
	);

	const chg2T = chg1.transform( chg2 );

	dynamic.append( chg1, chg2T )
	.assertText(
		'abcdef',
		'DEF',
		'ABC',
	);
}

/*
| Tests transformation of a list remove on a list remove.
*/
function transform_listRemove_listRemove_same( )
{
	console.log( '  * transform_listRemove_listRemove_same' );

	const dynamic =
		Dynamic.InitText(
			'abcdef',
			'123',
		);
	const tracePara0 = TraceRoot.text.add( 'paras',  0 );
	const chg1 =
		ChangeListMove.create(
			'traceFrom', tracePara0,
			'val', FabricPara.create( 'string', 'abcdef' )
		);

	dynamic.append( chg1 )
	.assertText(
		'123',
	);

	const chg2T = chg1.transform( chg1 );

	if( chg2T !== undefined ) throw new Error( );
}

/*
| Tests transformation of a string insertion on a list insertion.
*/
function transform_stringInsert_listInsert( )
{
	console.log( '  * transform_stringInsert_listInsert' );
	const dynamic = Dynamic.InitText( 'abcdef', '123' );
	const tracePara1 = TraceRoot.text.add( 'paras', 1 );

	const chg1 =
		ChangeListMove.create(
			'traceTo', tracePara1,
			'val', FabricPara.create( 'string', 'ABC' )
		);

	dynamic.append( chg1 )
	.assertText(
		'abcdef',
		'ABC',
		'123',
	);

	const chg2 =
		ChangeStringMove.create(
			'traceFrom', undefined,
			'traceTo', tracePara1.add( 'string' ).add( 'offset', 2 ),
			'val', 'XY'
		);

	dynamic.append( chg2 )
	.assertText(
		'abcdef',
		'12XY3',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T )
	.assertText(
		'abcdef',
		'ABC',
		'12XY3',
	);
}

/*
| Runs the test.
*/
def.static.run =
	function( )
{
	console.log( '* ListTransforms' );
	transform_listInsert_listInsert( );
	transform_listInsert_listRemove( );
	transform_listRemove_listRemove_same( );
	transform_stringInsert_listInsert( );
};
