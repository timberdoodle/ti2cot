/*
| The root of the test
*/
def.abstract = true;

import { Self as TestStringMoveBasics     } from '{Test/Suite/StringMoveBasics}';
import { Self as TestStringMoveTransforms } from '{Test/Suite/StringMoveTransforms}';
import { Self as TestListTransforms       } from '{Test/Suite/ListTransforms}';
import { Self as TestTextCombos           } from '{Test/Suite/TextCombos}';
//import { Self as TestSpecial              } from '{Test/Suite/Special}';

/*
| Runs the tests.
*/
def.static.init =
	async function( rootDir )
{
	await TestStringMoveBasics.run( );
	await TestStringMoveTransforms.run( );
	await TestListTransforms.run( );
	await TestTextCombos.run( );
	//await TestSpecial.run( );
};
