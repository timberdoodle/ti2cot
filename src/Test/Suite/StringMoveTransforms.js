/*
| Tests transformations by string move.
*/
def.abstract = true;

import { Self as ListMove   } from '{Change/List/Move}';
import { Self as Dynamic    } from '{Test/Dynamic}';
import { Self as FabricPara } from '{Test/Fabric/Para}';
import { Self as Helper     } from '{Test/Helper}';
// FIXME replace all by chgStringMove
import { Self as StringMove } from '{Change/String/Move}';
import { Self as TraceRoot  } from '{Test/Trace/Root}';

let chgStringMove;
//let logChg;

/*
| Tests an insert transforming a remove.
*/
function transformInsertRemove( )
{
	console.log( '  * transformInsertRemove' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'to', 0, 6,
			'val', 'es',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinxes of black quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 10,
			'val', 'black ',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of quartz, judge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinxes of quartz, judge my vow',
	);
}

/*
| Tests an insert transforming of a list remove.
|
| The transformation needs to change the value of the removal by the string move.
*/
function transformInsertListRemove( )
{
	console.log( '  * transformInsertListRemove' );
	const dynamic =
		Dynamic.InitText(
			'abcdef',
			'1234567890',
		);
	const chg1 =
		chgStringMove(
			'to', 1, 2,
			'val', 'xyz',
		);
	dynamic.append( chg1 ).assertText(
		'abcdef',
		'12xyz34567890',
	);
	const tracePara1 = TraceRoot.text.add( 'paras',  1 );
	const chg2 =
		ListMove.create(
			'traceFrom', tracePara1,
			'val', FabricPara.create( 'string', '1234567890' ),
		);
	dynamic.append( chg2 ).assertText(
		'abcdef'
	);

	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'abcdef',
	);
}

/*
| A move after a move should be unaffected.
*/
function transformMoveMoveAfter1( )
{
	console.log( '  * transformMoveMoveAfter1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0,  7,
			'to',   0, 16,
			'val', 'of ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx black of quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 24,
			'to',   0, 33,
			'val', 'judge ',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, my judge vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx black of quartz, my judge vow'
	);
}

/*
| Testing transformation of a move containting the same from but different to.
*/
function transformMoveMoveSame( )
{
	console.log( '  * transformMoveMoveSame' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'to',   0, 30,
			'val', 'black ',
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of quartz, judge black my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 10,
			'to',   0, 31,
			'val', 'bla',
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of ck quartz, judge mblay vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/// 'Sphinx of quartz, judge ck mblay vow',
		// FIXME also not the desired outcome
		/**/ 'Sphinx of quartz, judge black my vow',
	);
}

/*
| Test of a move transforming a move where it should be splitted.
*/
function transformMoveMoveSplit1( )
{
	console.log( '  * transformMoveMoveSplit1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
			/**/ 'The five boxing wizards jump quickly',
		);
	const traceString0 = TraceRoot.text.add( 'paras', 0 ).add( 'string' );
	const traceString1 = TraceRoot.text.add( 'paras', 1 ).add( 'string' );
	const traceString2 = TraceRoot.text.add( 'paras', 2 ).add( 'string' );
	const chg1 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 16 ),
			'traceTo',   traceString1.add( 'offset', 17 ),
			'val', 'quartz, '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of black judge my vow',
		'How quickly daft quartz, jumping zebras vex!',
		'The five boxing wizards jump quickly',
	);
	const chg2 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 13 ),
			'traceTo', traceString2.add( 'offset', 8 ),
			'val', 'ck qua'
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of blartz, judge my vow',
		'How quickly daft jumping zebras vex!',
		'The fiveck qua boxing wizards jump quickly',
	);
	const chgMove2T = chg1.transform( chg2 );
	//if( chgMove2T.length !== 2 ) throw new Error( );
	dynamic.append( chg1, chgMove2T ).assertText(
		'Sphinx of blajudge my vow',
		'How quickly daft rtz, jumping zebras vex!',
		'The fiveck qua boxing wizards jump quickly',
	);
}

/*
| Tests a move transforming a move where it should be splitted.
*/
function transformMoveMoveSplit2( )
{
	console.log( '  * transformMoveMoveSplit2' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 16,
			'to',   1, 4,
			'val', 'quartz, '
		);
	dynamic.append( chg1 ).assertText(
		////  00000000001111111111222222222233333333334444
		////  01234567890123456789012345678901234567890123
		/**/ 'Sphinx of black judge my vow',
		/**/ 'How quartz, quickly daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 20,
			'to',   1,  1,
			'val', 'tz, ju'
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333333344
		////  012345678901234567890123456789012345678901
		/**/ 'Sphinx of black quardge my vow',
		/**/ 'Htz, juow quickly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	//if( chg2T.length !== 2 ) throw new Error( );
	dynamic.append( chg1, chg2T ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black dge my vow',
		/**/ 'Htz, juow quarquickly daft jumping zebras vex!',
	);
}

/*
| Test of a move transforming a move where it should be splitted.
*/
function transformMoveMoveSplit3( )
{
	console.log( '  * transformMoveMoveSplit3' );
	const dynamic =
		Dynamic.InitText(
			'abcdefghijklmnopqrstuvwxyz',
			'0123456789',
			'ABCDEFGHIJKLMN',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 7,
			'to',   1, 5,
			'val', 'hijklmn',
		);
	dynamic.append( chg1 ).assertText(
		'abcdefgopqrstuvwxyz',
		'01234hijklmn56789',
		'ABCDEFGHIJKLMN',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 4,
			'to',   2, 8,
			'val', 'efghij',
		);
	dynamic.append( chg2 ).assertText(
		'abcdklmnopqrstuvwxyz',
		'0123456789',
		'ABCDEFGHefghijIJKLMN',
	);
	const chg2T = chg1.transform( chg2 );
	//if( chg2T.length !== 2 ) throw new Error( );
	dynamic.append( chg1, chg2T ).assertText(
		'abcdopqrstuvwxyz',
		'01234klmn56789',
		'ABCDEFGHefghijIJKLMN',
	);
}

/*
| Test of a move transforming a move where it should be splitted.
*/
function transformMoveMoveSplit4( )
{
	console.log( '  * transformMoveMoveSplit4' );
	const dynamic =
		Dynamic.InitText(
			////  00000000001111111111222222
			////  01234567890123456789012345
			/**/ 'abcdefghijklmnopqrstuvwxyz',
			/**/ '0123456789',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 7,
			'to',   1, 5,
			'val', 'hijklmn'
		);
	dynamic.append( chg1 ).assertText(
		////  00000000001111111111
		////  01234567890123456789
		/**/ 'abcdefgopqrstuvwxyz',
		/**/ '01234hijklmn56789',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 4,
			'to',   1, 8,
			'val', 'efghij'
		);
	dynamic.append( chg2 ).assertText(
		////  00000000001111111111
		////  01234567890123456789
		/**/ 'abcdklmnopqrstuvwxyz',
		/**/ '01234567efghij89',
	);
	const chg2T = chg1.transform( chg2 );
	//dynamic.append( chg1, chg2T.get( 0 ) ).assertText( );
	//// after change 1
	////  00000000001111111111
	////  01234567890123456789
	//// 'abcdopqrstuvwxyz'
	//// '01234hijklmn567efg89'
	//// needs to move 'hij' in second
	//// from 1  5
	//// to   1 15

	//if( chg2T.length !== 2 ) throw new Error( );
	dynamic.append( chg1, chg2T ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'abcdopqrstuvwxyz',
		// FIXME not the desired outcome!
		/**/ '01234klmn56hij7efg89'
	);
}

/*
| Tests a move transforming a move with encompassing contents.
*/
function transformMoveMoveEncompass( )
{
	console.log( '  * transformMoveMoveEncompass' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from',  0, 24,
			'to',    1, 14,
			'val', 'j'
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz, udge my vow',
		/**/ 'How quickly dajft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 23,
			'to',   0, 34,
			'val', ' judg'
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz,e my v judgow',
		/**/ 'How quickly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz,e my v judgow',
		/**/ 'How quickly daft jumping zebras vex!',
	);
}

/*
| Tests a move transforming a move with overlapping contents.
*/
function transformMoveMoveOverlappingFrom( )
{
	console.log( '  * transformMoveMoveOverlappingFrom' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 24,
			'to',   1, 12,
			'val', 'judge'
		);
	dynamic.append( chg1 ).assertText(
		////  00000000001111111111222222222233333333334
		////  01234567890123456789012345678901234567890
		/**/ 'Sphinx of black quartz,  my vow',
		/**/ 'How quickly judgedaft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 23,
			'to',   1,  7,
			'val', ' judge '
		);
	dynamic.append( chg2 ).assertText(
		////  0000000000111111111122222222223333333333444
		////  0123456789012345678901234567890123456789012
		/**/ 'Sphinx of black quartz,my vow',
		/**/ 'How qui judge ckly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		////  0000000000111111111122222222223333333333444
		////  0123456789012345678901234567890123456789012
		/**/ 'Sphinx of black quartz,my vow',
		/**/ 'How qui judge ckly daft jumping zebras vex!',
	);
}

/*
| Tests a move transforming a move with overlapping contents.
*/
function transformMoveMoveOverlappingDouble( )
{
	console.log( '  * transformMoveMoveOverlappingDouble' );
	const dynamic =
		Dynamic.InitText(
			////  000000
			////  012345
			/**/ 'Sphinx',
		);
	let chg1 =
		chgStringMove(
			'to',   0, 2,
			'val', 'A'
		);
	dynamic.append( chg1 ).assertText(
		////  0000000
		////  0123456
		/**/ 'SpAhinx',
	);
	let chg2 =
		chgStringMove(
			'from', 0, 1,
			'to',   0, 6,
			'val', 'ph'
		);
	dynamic.append( chg2 ).assertText(
		////  000000
		////  012345
		/**/ 'Sinxph',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/// 'Sphinx of black quartjudz,ge  my vow',
		/**/ 'SAinxph',
	);
}

/*
| Tests a move transforming a move with overlapping contents.
*/
function transformMoveMoveOverlappingDoubleLong( )
{
	console.log( '  * transformMoveMoveOverlappingDoubleLong' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 24,
			'to',   0, 22,
			'val', 'judge'
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartzjudge,  my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 21,
			'to',   0, 27,
			'val', 'z,'
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quart judz,ge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartjudge z, my vow'

//      FIXME REALLY?
//		/**/ 'Sphinx of black quartjudge ,z my vow'
	);
}

/*
| Tests a move transforming a move with remove.
*/
function transformMoveRemoveOverlapping( )
{
	console.log( '  * transformMoveRemoveOverlapping' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			////                              TTTTTT
			////                            DDDDD
			////                            SSMMMMMM
		);
	const chg1 =
		chgStringMove(
			'from', 0, 28,
			'to',   0, 9,
			'val', 'e my v'
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx ofe my v black quartz, judgow',
		////           DDD                    DD
	);
	const chg2 =
		chgStringMove(
			'from', 0, 26,
			'val', 'dge m'
		);
	dynamic.append( chg2 ).assertText(
		////  0000000000111111111122222222223
		////  0123456789012345678901234567890
		/**/ 'Sphinx of black quartz, juy vow',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		////  0000000000111111111122222222223
		////  0123456789012345678901234567890
		/**/ 'Sphinx ofy v black quartz, juow',
	);
}

/*
| Tests transformation of two identical moves.
| They should transform into one single move.
*/
function transformMoveDouble( )
{
	console.log( '  * transformMoveDouble' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0,  7,
			'to',   0, 30,
			'val', 'of ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx black quartz, judge of my vow'
	);
	const chg2T = chg1.transform( chg1 );
	//if( chg2T !== undefined ) throw new Error( );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx black quartz, judge of my vow'
	);
}


/*
| Tests a move transforming a string removal
| fully encapsulates the removal
*/
function transformMoveFullRemove( )
{
	console.log( '  * transformMoveFullRemove' );
	const dynamic =
		Dynamic.InitText(
			'abcdefghijkl',
			'0123456789',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 3,
			'to',   1, 2,
			'val', 'defgh',
		);
	dynamic.append( chg1 ).assertText(
		'abcijkl',
		'01defgh23456789',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 5,
			'val', 'fg',
		);
	dynamic.append( chg2 ).assertText(
		'abcdehijkl',
		'0123456789',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'abcijkl',
		'01deh23456789',
	);
}

/*
| Test a move-from-part transforming an insert.
*/
function transformMoveFromInsert( )
{
	console.log( '  * transformMoveFromInsert' );

	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1,  4,
			'to',   0, 10,
			'val', 'quickly '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of quickly black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'to', 1, 17,
			'val', 'nimbly '
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, judge my vow',
		'How quickly daft nimbly jumping zebras vex!',
	);
	const chgMove2T = chg1.transform( chg2 );
	dynamic.append( chg1, chgMove2T ).assertText(
		'Sphinx of quickly black quartz, judge my vow',
		'How daft nimbly jumping zebras vex!',
	);
}

/*
| Test transforming a string remove by a move from part.
*/
function transformMoveFromRemove( )
{
	console.log( '  * transformMoveFromRemove' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1, 4,
			'to',   0, 7,
			'val', 'quickly ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx quickly of black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 1, 17,
			'val', 'jumping ',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, judge my vow',
		'How quickly daft zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx quickly of black quartz, judge my vow',
		'How daft zebras vex!',
	);
}

/*
| Test transforming a string remove by a move from part.
*/
function transformMoveFromRemoveSplitFront( )
{
	console.log( '  * transformMoveFromRemoveSplit' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1, 4,
			'to',   0, 7,
			'val', 'quickly ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx quickly of black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 1, 1,
			'val', 'ow qu',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, judge my vow',
		'Hickly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx ickly of black quartz, judge my vow',
		'Hdaft jumping zebras vex!',
	);
}

/*
| Test transforming a string remove by a move from part.
*/
function transformMoveFromRemoveSplitInner1( )
{
	console.log( '  * transformMoveFromRemoveSplitInner1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0,  1,
			'to',   0, 10,
			'val', 'phinx',
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'S of phinxblack quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 0,
			'val', 'Sph',
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'inx of black quartz, judge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		' of inxblack quartz, judge my vow',
	);
}

/*
| Test transforming a string remove by a move from part.
*/
function transformMoveFromRemoveSplitInner2( )
{
	console.log( '  * transformMoveFromRemoveSplitInner2' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1, 4,
			'to',   0, 7,
			'val', 'quickly ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx quickly of black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 1, 5,
			'val', 'uick',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, judge my vow',
		'How qly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx qly of black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
}

/*
| Test transforming a string remove by a move from part.
*/
function transformMoveFromRemoveOverlapping( )
{
	console.log( '  * transformMoveFromRemoveOverlapping' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'to',   0, 30,
			'val', 'black ',
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of quartz, judge black my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 11,
			'val', 'lac',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of bk quartz, judge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx of quartz, judge bk my vow',
	);
}

/*
| Tests a move-to-part transforming an insert.
*/
function transformMoveToInsert ( )
{
	console.log( '  * transformMoveToInsert' );
	const dynamic =
		Dynamic.InitText(
			'abcdefghijklmnopqrstuvwxyz',
			'0123456789'
		);
	const chg1 =
		chgStringMove(
			'from', 0, 1,
			'to',   1, 3,
			'val', 'bcdefg',
		);
	dynamic.append( chg1 ).assertText(
		'ahijklmnopqrstuvwxyz',
		'012bcdefg3456789',
	);
	const chg2 =
		chgStringMove(
			'to',   1, 7,
			'val', 'XY',
		);
	dynamic.append( chg2 ).assertText(
		'abcdefghijklmnopqrstuvwxyz',
		'0123456XY789'
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'ahijklmnopqrstuvwxyz',
		'012bcdefg3456XY789'
	);
}

/*
| Test of move-to-part transforming a remove.
*/
function transformMoveToRemove( )
{
	console.log( '  * transformMoveToRemove' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1,  4,
			'to',   0, 10,
			'val', 'quickly ',
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of quickly black quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 7,
			'val', 'of black',
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx  quartz, judge my vow',
		'How quickly daft jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		//'Sphinx  quartz, judge my vow',
		// FIXME check
		'Sphinx quickly  quartz, judge my vow',
		'How daft jumping zebras vex!',
	);
}

/*
| Tests a remove transforming an insert.
*/
function transformRemoveInsert( )
{
	console.log( '  * transformRemoveInsert' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'val', 'black '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'to',   0, 24,
			'val', 'over'
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, overjudge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx of quartz, overjudge my vow'
	);
}

/*
| Tests a remove transforming an overlapping insert.
| 1: completely encapsulating.
*/
function transformRemoveInsertOverlapping1( )
{
	console.log( '  * transformRemoveInsertOverlapping1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'val', 'black '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'to', 0, 13,
			'val', 'blue'
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of blablueck quartz, judge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'Sphinx of bluequartz, judge my vow'
	);
}

/*
| Tests a remove transforming a move.
*/
function transformRemoveMove1( )
{
	console.log( '  * transformRemoveMove1' );
	const dynamic =
		Dynamic.InitText(
			'abcdefghijklmnopqrstuvwxyz',
			'0123456789',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 9,
			'val', 'jklmn',
		);
	dynamic.append( chg1 ).assertText(
		'abcdefghiopqrstuvwxyz',
		'0123456789',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 7,
			'to',   1, 5,
			'val', 'hijkl',
		);
	dynamic.append( chg2 ).assertText(
		'abcdefgmnopqrstuvwxyz',
		'01234hijkl56789',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'abcdefgopqrstuvwxyz',
		'01234hi56789',
	);
}

/*
| Tests a remove transforming a move.
*/
function transformRemoveMove2( )
{
	console.log( '  * transformRemoveMove2' );
	const dynamic =
		Dynamic.InitText(
			'abcdefghijklmnopqrstuvwxyz',
			'0123456789',
		);
	const traceString0 = TraceRoot.text.add( 'paras', 0 ).add( 'string' );
	const traceString1 = TraceRoot.text.add( 'paras', 1 ).add( 'string' );
	const chg1 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 9 ),
			'val', 'jklmn'
		);
	dynamic.append( chg1 ).assertText(
		'abcdefghiopqrstuvwxyz',
		'0123456789',
	);
	const chg2 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 12 ),
			'traceTo', traceString1.add( 'offset', 5 ),
			'val', 'mnop'
		);
	dynamic.append( chg2 ).assertText(
		'abcdefghijklqrstuvwxyz',
		'01234mnop56789',
	);
	const chg2T = chg1.transform( chg2 );
	dynamic.append( chg1, chg2T ).assertText(
		'abcdefghiqrstuvwxyz',
		'01234op56789'
	);
}

/*
| Tests a remove transforming a remove.
*/
function transformRemoveRemove( )
{
	console.log( '  * transfromRemoveRemove' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const traceString0 = TraceRoot.text.add( 'paras', 0 ).add( 'string' );
	const chg1 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 7 ),
			'val', 'of '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx black quartz, judge my vow',
	);
	const chg2 =
		StringMove.create(
			'traceFrom', traceString0.add( 'offset', 30 ),
			'val', 'my '
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of black quartz, judge vow',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		'Sphinx black quartz, judge vow'
	);
}

/*
| Tests a remove transforming a remove with overlapping contents.
*/
function transformRemoveRemoveOverlapping1( )
{
	console.log( '  * transformRemoveRemoveOverlapping1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 10,
			'val', 'black '
		);
	dynamic.append( chg1 ).assertText(
		'Sphinx of quartz, judge my vow',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 13,
			'val', 'ck qu'
		);
	dynamic.append( chg2 ).assertText(
		'Sphinx of blaartz, judge my vow',
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		'Sphinx of artz, judge my vow',
	);
}

/*
| Tests a remove transforming a remove with overlapping contents.
| (the other way around)
*/
function transformRemoveRemoveOverlapping2( )
{
	console.log( '  * transformRemoveRemoveOverlapping2' );
	const dynamic = Dynamic.InitText( 'abcdefghijklmnopqrstuvwxyz' );
	const chg1 =
		chgStringMove(
			'from', 0, 5,
			'val', 'fghijk'
		);
	dynamic.append( chg1 ).assertText(
		'abcdelmnopqrstuvwxyz'
	);
	const chg2 =
		chgStringMove(
			'from', 0, 2,
			'val', 'cdefgh'
		);
	dynamic.append( chg2 ).assertText(
		'abijklmnopqrstuvwxyz'
	);
	const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	dynamicT.assertText(
		'ablmnopqrstuvwxyz'
	);
}

/*
| Test of a ???
*/
function transformMoveMoveTouch( )
{
	console.log( '  * transformMoveMoveTouch' );
	const dynamic =
		Dynamic.InitText(
			//'abcdefghijklmnopqrstuvwxyz',
			//'0123456789',
			////  000000000011111111112222222222333333
			////  012345678901234567890123456789012345
			/**/ 'Sphinx of black quartz, judge my vow',
			/**/ 'How quickly daft jumping zebras vex!',
		);
	const chg1 =
		chgStringMove(
			'from', 1,  4,
			'to',   1, 27,
			'val', 'quick',
		);
	dynamic.append( chg1 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz, judge my vow',
		/**/ 'How ly daft jumping zequickbras vex!',
	);
	const chg2 =
		chgStringMove(
			'from', 1,  3,
			'to',   1, 14,
			'val', ' qui'
		);
	dynamic.append( chg2 ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz, judge my vow',
		/**/ 'Howckly da quift jumping zebras vex!',
	);
	const chg2T = chg1.transform( chg2 );
	//if( chg2T.length !== 2 ) throw new Error( );
	dynamic.append( chg1, chg2T ).assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		/**/ 'Sphinx of black quartz, judge my vow',
		/**/ 'Howly da quift jumping zeckbras vex!',
	);
}

/*
| Runs the test.
*/
def.static.run =
	function( )
{
	console.log( '* StringMoveTransformations' );
	chgStringMove = Helper.chgStringMove;
	//logChg = Helper.logChg;

	transformInsertRemove( );
	transformInsertListRemove( );
	transformMoveMoveAfter1( );
	transformMoveMoveEncompass( );
	transformMoveMoveOverlappingFrom( );
	transformMoveMoveOverlappingDouble( );
	transformMoveMoveOverlappingDoubleLong( );
	transformMoveRemoveOverlapping( );
	transformMoveMoveSame( );
	transformMoveMoveSplit1( );
	transformMoveMoveSplit2( );
	transformMoveMoveSplit3( );
	transformMoveMoveSplit4( );
	transformMoveDouble( );
	transformMoveFullRemove( );
	transformMoveFromInsert( );
	transformMoveFromRemove( );
	transformMoveFromRemoveSplitFront( );
	transformMoveFromRemoveSplitInner1( );
	transformMoveFromRemoveSplitInner2( );
	transformMoveFromRemoveOverlapping( );
	transformMoveToInsert( );
	transformMoveToRemove( );
	transformRemoveInsert( );
	transformRemoveInsertOverlapping1( );
	transformRemoveMove1( );
	transformRemoveMove2( );
	transformRemoveRemove( );
	transformRemoveRemoveOverlapping1( );
	transformRemoveRemoveOverlapping2( );
	transformMoveMoveTouch( );
};
