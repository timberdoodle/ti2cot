/*
| Tests transformations by string move.
*/
def.abstract = true;

import { Self as Dynamic              } from '{Test/Dynamic}';
import { Self as Helper               } from '{Test/Helper}';

let chgStringMove;
let logChg;

/*
| Tests a move transforming a move with overlapping contents.
*/
function transformSpecial1( )
{
	console.log( '  * transformSpecial1' );
	const dynamic =
		Dynamic.InitText(
			////  000000000011111
			////  012345678901234
			/**/ 'Sphinx-of.black',
		);
	const chg1 =
		chgStringMove(
			'from', 0, 5,
			'to',   0, 10,
			'val', 'x'
		).explode;
	logChg( chg1, 'chg1' );
	dynamic.append( chg1 ).assertText(
		////  000000000011111
		////  012345678901234
		/**/ 'Sphin-of.xblack',
	);
	const chg2 =
		chgStringMove(
			'from', 0, 5,
			'to', 0, 1,
			'val', 'x-'
		).explode;

	// 012345678901234
	// Sxphin-of.black

	// 6 to 1

	logChg( chg2, 'chg2' );
	dynamic.append( chg2 ).assertText(
		////  000000000011
		////  012345678901
		/**/ 'Sx-phinof.black',
	);

	const chg20 = chg2.get( 0 );
	const chg21 = chg2.get( 1 );

	const chg21N = chg20.reversed.transform( chg21 );

	/*
	logChg( chg21N, 'chg21N' );
	const chg21NN = chg20.transform( chg21N );
	logChg( chg21NN, 'chg21NN' );
	*/

	const chg20T = chg1.transform( chg20 );

	//const chg1T0 = chg20.transform( chg1 );
	//const chg21NT = chg1T0.transform( chg21N );
	// or simply?
	const chg21NT = chg1.transform( chg21N );

	const chg21T = chg20T.transform( chg21NT );

	const dynamicT = dynamic.append( chg1, chg20T, chg21T );
	dynamicT.assertText( );



	/*
	let chg2T;
	{
		const list = [ ];
		let self = chg1;
		for( let c of chg2 )
		{
			const cr = self.transform( c );
			self = c.transform( self );
			logChg( self, 'SELFT' );
			if( !self ) break;
			if( !cr ) continue;
			if( cr.ti2ctype === ChangeList )
			{
				for( let ch of cr ) list.push( ch );
			}
			else
			{
				list.push( cr );
			}
		}
		chg2T = ChangeList.Array( list );
	}
	logChg( chg2T, 'chg2T' );
	*/


	/*
	//const chg2T = chg1.transform( chg2 );
	const dynamicT = dynamic.append( chg1, chg2T );
	// What really should be the result of this messy respective
	// intertanglement is debatable, for now its okay
	// if the transformations keep consistent
	dynamicT.assertText(
		////  000000000011111111112222222222333333
		////  012345678901234567890123456789012345
		     '',
	);
	*/
}

/*
| Runs the test.
*/
def.static.run =
	function( )
{
	console.log( '* StringMoveTransformations' );
	chgStringMove = Helper.chgStringMove;
	logChg = Helper.logChg;

	transformSpecial1( );
};
