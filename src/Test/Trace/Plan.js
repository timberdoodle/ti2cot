/*
| Plan of traces.
*/
def.abstract = true;

import { Self as FabricNote  } from '{Test/Fabric/Note}';
import { Self as FabricPara  } from '{Test/Fabric/Para}';
import { Self as FabricSpace } from '{Test/Fabric/Space}';
import { Self as FabricText  } from '{Test/Fabric/Text}';
import { Self as Plan        } from '{ti2c:Plan}';

/*
| Items of a space.
*/
def.staticLazy.items = ( ) =>
	Plan.Build( {
		name: 'items',
		key: true,
		subs:
		{
			text: Self.text,
		},
		types:
		[
			FabricNote,
		],
	} );

/*
| A space.
*/
def.staticLazy.space = ( ) =>
	Plan.Build( {
		name: 'space',
		subs:
		{
			items: Self.items,
		},
		types: [ FabricSpace ],
	} );

/*
| A text
*/
def.staticLazy.text = ( ) =>
	Plan.Build( {
		name: 'text',
		subs:
		{
			paras:
			{
				at: true,
				subs:
				{
					string:
					{
						subs: { offset: { at: true } },
					}
				},
				types: [ FabricPara ],
			}
		},
		types: [ FabricText ],
	} );
