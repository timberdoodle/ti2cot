/*
| Shortcuts for trace roots.
*/
def.abstract = true;

import { Self as Plan  } from '{Test/Trace/Plan}';
import { Self as Trace } from '{ti2c:Trace}';

def.staticLazy.space = ( ) => Trace.root( Plan.space );
def.staticLazy.text = ( ) => Trace.root( Plan.text );
