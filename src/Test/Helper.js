/*
| Helper functions for the test suite.
*/
def.abstract = true;

import { Self as ListMove   } from '{Change/List/Move}';
import { Self as Sequence   } from '{Change/Sequence}';
import { Self as Simulcast  } from '{Change/Simulcast}';
import { Self as StringMove } from '{Change/String/Move}';
import { Self as TraceRoot  } from '{Test/Trace/Root}';

/*
| Logs a change in a reduced way for these test context.
*/
def.static.logChg =
	function( chg, label, compact )
{
	if( label === undefined ) label = '';

	switch( chg.ti2ctype )
	{
		case ListMove:
		{
			console.log( label + ' list move' );
			const from = chg.traceFrom;
			const to = chg.traceTo;
			const val = chg.val;
			if( from )
			{
				console.log( '  from @ ' + from.asString );
			}
			if( to )
			{
				console.log( '  to   @ ' + to.asString );
			}
			console.log( '  val ' + val.jsonfy( ) );
			if( !compact )
			{
				console.log( );
			}
			return;
		}

		case Sequence:
		{
			const chglen = chg.length;
			console.log( label + ' seq # ' + chglen );
			for( let c = 0; c < chglen; c++ )
			{
				Self.logChg( chg.get( c ), label + '[' + c + '] ', true );
			}
			if( !compact )
			{
				console.log( );
			}
			return;
		}

		case Simulcast:
		{
			const chglen = chg.length;
			console.log( label + ' sim # ' + chglen );
			for( let c = 0; c < chglen; c++ )
			{
				Self.logChg( chg.get( c ), label + '[' + c + '] ', true );
			}
			if( !compact )
			{
				console.log( );
			}
			return;
		}

		case StringMove:
		{
			console.log( label + ' string move' );
			const traceFrom = chg.traceFrom;
			const traceTo = chg.traceTo;
			const val = chg.val;
			if( traceFrom )
			{
				console.log( '  from @ ' + traceFrom.get( 1 ).at + ' / ' + traceFrom.get( 3 ).at );
			}
			if( traceTo )
			{
				console.log( '  to   @ ' + traceTo.get( 1 ).at + ' / ' + traceTo.get( 3 ).at );
			}
			console.log( '  val   >' + val + '<' );
			if( !compact )
			{
				console.log( );
			}
			return;
		}

		default: throw new Error( );
	}
};

/*
| Creates a string move in context of these texts.
|
| ~args:
|   'from' [LINE] [OFFSET]
|   'to'   [LINE] [OFFSET]
|   'val'  [VALUE]
*/
def.static.chgStringMove =
	function( ...args )
{
	let from, to, val;
	let a = 0, alen = args.length;
	while( a < alen )
	{
		switch( args[ a ] )
		{
			case 'from':
				from =
					TraceRoot
					.text
					.add( 'paras', args[ a + 1 ]  )
					.add( 'string' )
					.add( 'offset', args[ a + 2 ] );
				a += 3;
				continue;

			case 'to':
				to =
					TraceRoot
					.text
					.add( 'paras', args[ a + 1 ]  )
					.add( 'string' )
					.add( 'offset', args[ a + 2 ] );
				a += 3;
				continue;

			case 'val':
				val = args[ a + 1 ];
				a += 2;
				continue;

			default: throw new Error( );
		}
	}

	return(
		StringMove.create(
			'traceFrom', from,
			'traceTo', to,
			'val', val,
		)
	);
};
